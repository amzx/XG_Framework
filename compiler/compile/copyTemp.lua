--[[
    Builder 注入Jass 启动lua

]]
--复制临时项目
function builder:copyTempProject()
    local input =  Root .. '\\maps\\' ..  self.map
    local output = Root .. '\\debug\\' ..  self.map  .. '\\temp'

    mkdir(output..'\\map\\')
    if self.type ~= 'debug' then
        xcopy(
            Root ..  '\\xLua',
            output .. '\\map\\xLua'
        )
    else
        mkdir(output..'\\map\\xLua\\')
        mkdir(output..'\\map\\xLua\\cache')
        copy(
            Root ..  '\\xLua\\Main.lua',
            output .. '\\map\\xLua\\Main.lua'
        )
        copy(
            Root ..  '\\xLua\\Version.lua',
            output .. '\\map\\xLua\\Version.lua'
        )

    end
    xcopy(
        Root ..  '\\Resource',
        output .. '\\map'
    )

    xcopy(
        input ..  '\\scripts',
        output .. '\\map'
    )
    copy(
        input ..  '\\.w3x',
        output .. '\\.w3x'
    )

    --先解压xLua resource、以达到优先级最低的设定
    local dir = { 'map', 'table', 'resource', 'trigger', 'w3x2lni', 'sound' }
    print('[2]复制临时项目以打包地图...')
    print('   目录:', table.concat(dir,'    ') )
    local filecount = {}
    for index,value in ipairs(dir) do
        mkdir( ( output .. '\\'.. value ) ) --创建w3x2lni所需文件夹

        local result = xcopy( string.format('"%s"\\%s', input, value),  string.format('"%s"\\%s', output, value)  )

        filecount[index] = result:match( ("复制了%s*(%d-)%s*个文件"):u2a() )

    end
    local str = ''
    for index, value in ipairs(filecount) do
        local s = value .. (' '):string( (dir[index]:len() + 4) - value:len() )
        str = str .. (s or 'x')
    end
    print("   数量:", str ) --覆盖项目下的map

end