


--- 移除地图内脚本
local function removeLua(self)
    rd( self.temp .. '\\map\\xLua\\module' )

end

--- 注入环境
local function injectEnv(self)
    local lua = { [0]=1, 'package.path = ""'}
    local pack = [=[package.path = package.path .. ';%s']=]
    local pathList = {
        --- 按优先级排序
        --地图 地图里有当然是地图的，因为地图里的都是缓存的lua
        "?.lua",
        "?\\init.lua",

        --项目
        self.path_map .. '\\scripts\\?.lua',
        self.path_map .. '\\scripts\\?\\init.lua',
        --resource
        Root .. '\\Resource\\?.lua',
        Root .. '\\Resource\\?\\init.lua',
        --框架
        Root .. '\\?.lua',
        Root .. '\\?\\init.lua',

        --框架前缀省略
        Root .. "\\xlua\\?.lua",
        Root .. "\\xlua\\?\\init.lua",
        --debug
        Root .. '\\compiler\\debug\\?.lua',
        Root .. '\\compiler\\debug\\?\\init.lua',
    }
    for i, path in ipairs(pathList) do
        table_insert( lua, pack:format( path ):replace( '\\','\\\\' ) )
    end
    table_insert(lua, [=[
        package.path = package.path:sub(2)
        require 'jass.runtime'.debugger = 4279
        require 'Hotfix'
    ]=] )


    local env = io.open( self.temp .. '\\map\\xLua\\Version.lua',"a+b" ) --追加

    assert(env,  '调试模式注入失败。'  )

    env:write( table.concat(lua,'\n') )
    env:close()

end

return function (self)
    
    removeLua(self)

    injectEnv(self)

end