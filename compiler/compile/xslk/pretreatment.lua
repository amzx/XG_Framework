--预处理
--[[
xslk.item:new {
    name = '攻击指环',
    desc = --动态文本
[=[@ATK1@
@ARM1@
@HP1@
被动：攻击有5%概率造成${伤害}物理伤害
]=],
    
    ATTR = {
        { 'ATK1', '攻击+10' },
        { 'ARM1', '护甲 + 1' },
        { 'HP1' ,'生命 - 5'},
    },
}

xslk.item:rule {
        --描述文本 string(pattern)|func(table) 当为函数时取函数返回值 否则直接采用desc
        desc = function (params)
            local atk = params[1]
            local sign = '-'
            if atk > 0 then
                sign = '+'
            end
            return ( '%s%u 攻击' ):format( sign, atk )
        end,

        --正则表达式
        regexp = '攻击%s-[%+%-]%s-(%d+)',

        --标识 技能|模拟|混合
        --使用技能则表示完全使用技能数据,为每个属性创建技能
        --使用模拟则表示完全由作者模拟属性，系统不会自动处理这些数据
        --使用混合则表示技能+修改数据以最少技能达到效果
        flag = '模拟',

        --nil|table 预设技能
        abil = xslk.ability.new {  },
        data = 'DataA',

}

]]
local _mod = mod:load('XSLK\\ITEM\\DESC')

local rules
local function rule( self, params )
    table_insert( rules, params )
    if params.flag == '混合' then
        --混合开关 用于判断 注入代码
        _mod.Mix = true
    end
end

local method = {}
setmetatable(method, {
    __index = function (self,key)
        assert( nil, ( 'xslk->flag->method key:%s 不存在发生错误' ):format( tostring(key) ) )
    end
})

method['技能'] = function ( rule, item, value )
    local list = item.abilList or ''
    if #list > 0 then
        list = list .. ','
    end
    --为物品添加技能
    list = list .. xslk.ability:new { _parent = rule.abil, [rule.data] = value }
    item.abilList = list

    
end

method['模拟'] = function ( rule, item )
    
end

method['混合'] = function ( rule, item )
    --编译时无需动作，在框架运行时去修改技能数据
    local slk = xslk.ability.lni._ini[rule.abil]
    slk.levels = 2

    --未有任何修改的原版物编 修改时将其算入静态
    if not slk._parent then
        slk._parent = rule.abil
        xslk.ability.count_static = xslk.ability.count_static + 1
    end

end




local function processing( mt, item )
    local attrs = item.ATTR
    local map_rule = {}
    local map_attr = {}
    for index, attr in ipairs(attrs) do

        for _, rule in ipairs(rules) do
            --物品自动添加技能
            value = rule.match( attr[2] )
            if value then
                method[ rule.flag ] ( rule, item, value )
                map_rule[ attr[1] ] = rule
                map_attr[ attr[1] ] = attr

                break --成功匹配属性，跳到下一属性
            end
        end

    end

    ---@type string 生成物品说明
    local str = item.desc
    if str then


        local pos_at
        local pos,len = 1,str:len()
        --返回跳过的长度 字符串(不包含符号)
        local at = function ()
            local start = pos
            pos = pos + 1
            while pos <= len do

                local chr = str:sub( pos, pos )

                if chr == '\\' then --转义
                    pos = pos + 1
                elseif chr == '@' then
                    return pos - start, --跳过字符
                    str:sub( start+1, pos-1 )   --字符串
                end

                pos = pos + 1

            end

            return len,''
        end

        local skip,str_at
        while pos <= len do
            local chr = str:sub( pos, pos )

            if chr == '@' then
                skip,attr = at()

                local desc_rule = map_rule[ attr ]
                assert(desc_rule,('物编动态属性: 属性规则[%s]未定义！'):format(attr))
                desc_rule = desc_rule.desc
                local newStr
                assert(map_attr[attr], ('err: 物品动态说明 属性参数[%s]不存在!'):format(attr) )

                local desc_attr = map_attr[attr][2]  --ATTR = { 'ATK1', '攻击+10' }

                if type(desc_rule) == 'function' then
                    local params = Params2Table( map_rule[attr].match( desc_attr )  )
                    newStr = desc_rule( params )

                elseif type(desc_rule) == 'string' then
                    newStr = desc_rule:format( map_rule[attr].match( desc_attr )  )

                else
                    assert(false, ('err: rule.desc 类型不支持,仅支持function与string!'):format(attr) )
                end

                str = str:sub( 1, pos-skip-1 ) .. newStr .. str:sub( pos+1 )

                local offset = ( #newStr - skip )

                pos = pos + offset
                len = len + offset

            end

            pos = pos + 1
        end

        item.Description = str
        item.Ubertip = str
    end
end

function builder:itemPreInit()

rules = {}
xslk.item.rules = rules
--- 添加新规则
xslk.item.rule = rule
xslk.item.processing = processing

xslk.item.mod = _mod

--模拟的话 覆写method['模拟']
xslk.item.method = method

end