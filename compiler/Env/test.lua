-- -- -- -- -- -- -DEBUG 测试版-- -- -- -- -- -- -- -
local runtime	= require 'jass.runtime'
print = require ("jass.console").write
local o_require = require

local req_lv = 0

require = function(lib)
    local module = package.loaded[ lib ]
    if module then
        return module
    end
    
    local prefix = string.rep('->  ', req_lv)
    req_lv = req_lv + 1
    print(prefix .. 'require ', lib, ' Init...')
    
    local suc, tmp = xpcall(o_require, runtime.error_handle, lib)
    
    print(prefix .. 'require ', lib, ' Inited')
    req_lv = req_lv - 1
    return tmp
end
runtime.sleep = false
require ("jass.console").enable = true

printf = function (pattern, ...)
    print( string.format( pattern, ... ) )
end

runtime.handle_level = 0
runtime.catch_crash = true
runtime.error_handle = function(msg)
	print("-------------error_handle--------------")
	print(tostring(msg) .. "\n")
	print(debug.traceback())
	print("---------------------------------------")
end

--添加path后 require 就可以不加框架目录了
package.path = package.path .. ";?\\init.lua"
package.path = package.path .. ";xlua\\?.lua"
package.path = package.path .. ";xlua\\?\\init.lua"

xLua.Env = 'test'
