-- -- -- -- -- -- -DEBUG 测试版-- -- -- -- -- -- -- -
print 'debug'
local string_rep = string.rep
local runtime	= require 'jass.runtime'
runtime.sleep = false
require ("jass.console").enable = true
print = require ("jass.console").write
local o_require = require
local print = print
local req_lv = 0

require = function(lib)
    local module = package.loaded[ lib ]
    if module then
        return module
    end
    
    local prefix = string_rep('->  ', req_lv)
    req_lv = req_lv + 1
    print(prefix .. 'require ', lib, ' Init...')
    
    local suc, tmp = xpcall(o_require, runtime.error_handle, lib)
    
    print(prefix .. 'require ', lib, ' Inited')
    req_lv = req_lv - 1
    return tmp
end


printf = function (pattern, ...)
    print( string.format( pattern, ... ) )
end

runtime.handle_level = 0
runtime.catch_crash = true
runtime.error_handle = function(msg)
	print("-------------error_handle--------------")
	print(tostring(msg) .. "\n")
	print(debug.traceback())
	print("---------------------------------------")
end

xLua.Env = 'debug'