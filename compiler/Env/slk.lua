-- -- -- -- -- -- -Release 正式版-- -- -- -- -- -- -- -
local runtime	= require 'jass.runtime'
runtime.sleep = false
require ("jass.console").enable = false

function print(...)
end

printf = function (pattern, ...)
    --print( string.format( pattern, ... ) )
end

runtime.handle_level = 0
runtime.catch_crash = false

--添加path后 require 就可以不加框架目录了
package.path = package.path .. ";?\\init.lua"
package.path = package.path .. ";xlua\\?.lua"
package.path = package.path .. ";xlua\\?\\init.lua"