
function getString(str, pos, max, simp)
    simp = simp or '"'
    local start = pos
    pos = pos + 1
    while pos <= max do
        local chr = str:sub(pos,pos)
        if chr == '\\' then
            pos = pos + 1
        elseif chr == simp then
            return pos - start
        end
        pos = pos + 1
    end
    return 1
end

function getParam(str, pos, max)
    pos = pos + 1
    local start = pos
    local pSt = start
    local count = 0
    local params = {}
    max = max or #str
    while pos <= max do
       local chr = str:sub(pos,pos)
       if chr == ',' then
           --新参数开始点 非常量
           --print( params, str:sub( pSt, pos-1 ) )
           pSt = pos + 1
           
       elseif chr == ')' then
           --函数结束
           return pos - start, params
       elseif chr == ' ' then
           
       else
            --新表达式
            local skip = getExpression(str, pos, max)
            pos = pos + skip
            table.insert(params, str:sub(pSt,pos))

       end
       pos = pos + 1
    end
    print('err params', pos, max)
    return 1,{}
end

---@return number,table,number 跳过字符数，参数(0为函数名), 结束位置
function getExpression(str, pos, max)  --匹配参数   返回 结束位置
    pos = pos or 1
    max = max or #str
    local start = pos
    local funcName = ''
    local params,skip = {}
    while pos <= max do
        local chr = str:sub(pos,pos)
        if chr == '(' then
            if funcName ~= '' then
                params[0] = funcName
                skip,params = getParam(str, pos, max)
                skip = skip + 1 --跳过)括号
            else
                skip,params = getParam( str, pos,max )
                skip = skip + 1 --跳过)括号
            end
            pos = pos + skip
            return pos - start, params, pos
        elseif chr == '"' or chr == "'" then
            skip = getString(str, pos, max, chr)
            pos = pos + skip
            table.insert( params, str:sub(start,pos) )
            return pos - start, params, pos
        elseif chr == ' ' then
            
        elseif chr == ')' or chr == ',' or pos > max then
           --常量结束
           table.insert( params, str:sub( start, pos - 1 ) )
           return pos - start - 1 , params , pos
        else
            funcName = funcName .. chr
        end
        
        pos = pos + 1
    end
    table.insert( params, str )
    return pos - start , params, pos
end
