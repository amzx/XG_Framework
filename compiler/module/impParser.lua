---@author:雪月灬雪歌  2023-05-21 18:28
---@version 1.0
--[[

    雪月框架    -   导入文件分析

]]

--文件内存读取模块
local mem = require 'module.memory'
local fs = require 'bee.filesystem'
local stormlib = require 'ffi.stormlib'


return function (path_w3r)
     local file = io.open(path_w3r, 'a+b')
    if not file then
        return  --w3r文件不存在
    end
    local w3r = file:read('*a')
    file:close()

    local w3x = stormlib.open(fs.path(path_w3r))

    if not w3x then
        print("warning: 分析资源文件时无法读取地图,请检查地图是否被MPQ工具占用")
        return
    end

    local new = w3x:load_file("war3map.imp")

    read(4) -- skip
    local maxCount = read_int()
    local rects = { }
    for i = 1, maxCount do
        local LB = { read_float(), read_float() }
        local RT = { read_float(), read_float() }
        local name = read_string()
        local id = read_int() -- 0 ~ maxCount-1
        local weather = read_int()
        local sound = read_string() --音效路径
        local colorB = read(1)
        local colorG = read(1)
        local colorR = read(1)
        local colorA = read(1)
        rects[ i ] = {
            LB = LB,
            RT = RT,
            name = name,
            id = id,
            weather = weather,
            sound = sound,
            color = { A = colorA , R = colorR, G = colorG, B = colorB },
        }
        
    end
    --  到此w3r所有字节读取完成
    return rects
end