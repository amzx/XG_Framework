---@author:雪月灬雪歌  2022-03-11 19:20:27
---@version 1.2
--[[

    雪月框架    -   xRect解析器

]]

--文件内存读取模块
local mem = require 'module.memory'


return function (path_w3r)
     local file = io.open(path_w3r, 'a+b')
    if not file then
        return  --w3r文件不存在
    end
    local w3r = mem:loadFormString( file:read('*a') )
    file:close()

    w3r:read(4) -- skip
    local maxCount = w3r:read_int32()
    local rects = { }
    for i = 1, maxCount do
        local LB = { w3r:read_float32(), w3r:read_float32() }
        local RT = { w3r:read_float32(), w3r:read_float32() }
        local name = w3r:read_string()
        local id = w3r:read_int32() -- 0 ~ maxCount-1
        local weather = w3r:read_int32()
        local sound = w3r:read_string() --音效路径
        local colorB = w3r:read(1)
        local colorG = w3r:read(1)
        local colorR = w3r:read(1)
        local colorA = w3r:read(1)
        rects[ i ] = {
            LB = LB,
            RT = RT,
            name = name,
            id = id,
            weather = weather,
            sound = sound,
            color = { A = colorA , R = colorR, G = colorG, B = colorB },
        }
    end
    --  到此w3r所有字节读取完成
    return rects
end