local ui = require 'gui.new.template'

local window = builder.window

local map = nil

local view, data

local interface = {
    style = { FlexGrow = 1, FlexDirection = 'row', AlignItems = 'center', JustifyContent = 'center' },
    
    ui.label {
        text = '选择你要编译的项目:',--[[lang.ui.DRAG_MAP,]]
        style = { Position = 'absolute',Top = 0, Left = 5, Height = 40, AlignItems = 'center', JustifyContent = 'center'  },
        font = { size = 20, weight = 'bold' },
        text_color = '#cccccc',
    },
    ui.button {
        title = '编译上一次的地图',
        font = { size = 15 },
        style = { Position = 'absolute', Bottom = 10, Left = 15, Height = 48, Width = 140 },
        on = {
            click = function()
                window._window:close()
                builder:lastBuild()
            end
        }
    },
    ui.button {
        title = '测试上一次的地图',
        font = { size = 15 },
        style = { Position = 'absolute', Bottom = 10, Right = 15, Height = 48, Width = 140 },
        on = {
            click = function()
                window._window:close()
                builder.map = builder.cfg._ini.Builder.LastMap
                builder.type = 'last'
                builder:build_info()
                builder:lastTest()
            end
        }
    },

}

local sOut = io.popen('dir /Ad /B "' .. unicode.u2a (Root) .. '\\maps\\*"', "r") 
local sData = unicode.a2u( sOut:read "*a" )
local arr = sData:split("\n")
local x,y = 0,0
local max_i = #arr - 1
local rows = 5
table.remove(arr, max_i+1)
if max_i > 10 then
    rows = 10 --超过10个项目 扩大界面
    window._window:setcontentsize {  width = 500,height = 650 }
elseif max_i > 5 then
    rows = 5 --超过10个项目 扩大界面
    window._window:setcontentsize {  width = 500,height = 400 }
end
for i =1, max_i do
    x = x + 1
    if x > rows then
        x = 1; y=y+1
    end
    table.insert(interface,ui.button
        {
            title = arr[i] or ' ',
            font = {
                size = 18,
            },
            color='#3c3b3a',
            color_hover = '#323232',
            style = { color = '#f0f0f0',Position = 'absolute', Top = 40 + (x-1) * 50, Left = 40 + y * 202, Width = 200, height = 48, },
            on = {
                click = function()
                    builder.map = arr[i]
                    --view:setvisible(false)
                    map = window:show_page('map')
                end
            }
        }
    )
end

local ev = require 'gui.event'
ev.on('update theme', function(color, title)
end)
local template = ui.container(interface)

ui.button {
        title = 'V1.0',
        color = '#333743',
        style = { Position = 'absolute', Bottom = 0, Right = 0, Width = 140 },
        on = {
            click = function()
                --window:show_page('about')
            end
        }
    }

view, data = ui.create(template, {
    color = '#222',
})



return view
