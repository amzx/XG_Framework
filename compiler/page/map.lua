local ui = require 'gui.new.template'
local window = builder.window
local view, data

local interface = {
    style = { FlexGrow = 1, FlexDirection = 'row', AlignItems = 'center', JustifyContent = 'center' },
    ui.label {
        text = '选择你要编译的类型： [SLK带有lua加密]',
        style = { Position = 'absolute',Top = 0, Left = 5, Height = 60, AlignItems = 'center', JustifyContent = 'center'  },
        font = { size = 20, weight = 'bold' },
        text_color = '#cccccc',
    },

}

local x,y = 0,0
local rows = 5
if #TYPE > 10 then
    rows = 10 --超过10个项目 扩大界面
    window._window:setcontentsize {  width = 500,height = 650 }
end
for _,v in ipairs(TYPE_id) do
    x = x + 1
    if x > rows then
        x = 1; y = y + 1
    end
    table.insert(interface,ui.button
        {
            title = TYPE[v],
            font = {
                size = 18
            },
            color='#3c3b3a',
            color_hover = '#323232',
            style = {color = '#f0f0f0', Position = 'absolute', Top = 20 + x * 50 + 5, Left = 40 + y*140, Width = 140, height = 48, },
            on = {
                click = function()
                    builder.type = v
                    --关闭GUI
                    window._window:close()

                    --执行编译过程()
                    builder:exec()
                end
            }
        }
    )
end
x = x + 1
if x > rows then
    x = 1; y = y + 1
end
table.insert(interface,ui.button
    {
        title = '用编辑器打开',
        font = {
            size = 18
        },
        color='#3c3b3a',
        color_hover = '#323232',
        style = { color = '#f0f0f0',Position = 'absolute', Top = 20 + x * 50 + 5, Left = 50 + y*140, Width = 140, height = 48, },
        on = {
            click = function()
                --关闭GUI
                window._window:close()

                builder:editor()

            end
        }
    }
)

local template = ui.container(interface)


view, data = ui.createEx(template, {
    color = '#222',
})

return view
