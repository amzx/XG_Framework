--技能相关扩展
local EXGetAbilityState = japi.EXGetAbilityState
local EXSetAbilityState = japi.EXSetAbilityState
local EXGetUnitAbility = japi.EXGetUnitAbility --获取 技能对象
---@class mAbility 技能模块
local mAbility = {
    type = 'module',
    module = 'ability',
}
mAbility.__index = mAbility
---@class unit
local cUnit = unit.cUnit

---@class cAbility
---@field id string 技能id
---@field _id int
---@field units unit[]
---@field maxLevel int 技能最大等级
---@field Real table 实数修改记录
---@field Integer table 整数修改记录
---@field String table 字符串修改记录
local cAbility = {
    type = 'class',
    class = 'ability',

}
cAbility.__index = cAbility

mAbility.cAbility = cAbility

---@ 来自 unit:init 的事件响应
local module_abil= unit:event_any( '单位-初始化',function (event, params)
    local u = params.unit
    --新建技能类
    mAbility:init( u )
end)

---@ 创建技能类给unit
---@param abil_id string 技能id
---@param u unit 绑定unit
---@return ability|nil 如果传入的单位不存在或技能id错误则返回nil
function mAbility:new( abil_id, u )
    local obj
    if u then
        obj = self:generate(abil_id)
        table_insert( obj.units, u )
        cj.UnitAddAbility( u.handle , obj._id )
    end
    return obj
end

---@private 生成技能
---@return ability 技能
function mAbility:generate( abil_id )
    local slk = xslk.ability( abil_id )
    ---@class ability : cAbility
    local t = {
        type = 'ability', --技能对象
        id = abil_id, --技能id
        _id = base.iid(abil_id),

        level = 1,
        maxLevel = slk.levels,
        hero = slk.hero,

        units = {},

        --技能修改记录
        --State = {}, --仅有CD
        Real = {},
        Integer = {},
        String = {},

    }
    setmetatable( t, cAbility )

    return t
end

---@private 初始化技能列表
---@param u unit 该类的技能列表初始化为u的技能
function mAbility:init( u )
    local slk = xslk.unit( u.id )
    local str_abils = (slk.abilList or '') .. (slk.heroAbilList or '')
    local abils = str_abils:split(',')
    u.abilitys = {}
    for i, v in ipairs(abils) do
        ---因为技能列表本就是u所携带的 只生成初始技能信息 无需添加技能
        local abil = self:generate( v )
        table_insert(u.abilitys, abil )
    end
end


---@ 给单位添加技能
---@param self unit
---@param abil_id string
---@return ability
function cUnit:addAbil( abil_id )
    local abil = mAbility:new( abil_id, self )
    --动态缓存 刷新单位属性

    ---------------------
    
    return abil
end



----------------------------------------


----------------------------------------


function cAbility:getCD()
    for _, unit in ipairs(self.units) do
        return EXGetAbilityState( EXGetUnitAbility(unit, self._id ), 1 ) or 0
    end
    return 0
end
function cAbility:setCD( cd )
    for _, unit in ipairs(self.units) do
        EXSetAbilityState( EXGetUnitAbility(unit, self._id ), 1, cd )
    end
    return true
end

function cAbility:getLevel(  )
    return self.level
end

function cAbility:setLevel( level )
    self.level = level
    for _, unit in ipairs(self.units) do
        cj.SetUnitAbilityLevel(unit, self._id, level )
    end
end


function cAbility:del()
    local aid = self._id
    for _, u in ipairs(self.units) do
        cj.UnitRemoveAbility( u.handle, aid )
    end
    setmetatable(self,nil)
    self.units = nil
    self.Integer = nil
    self.Real = nil
    self.String = nil
end


return mAbility