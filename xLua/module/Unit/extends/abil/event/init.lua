
---@type cAbility
local cAbility = ability.cAbility

local pool = eventpool:new('技能-事件池')

---@private 通知事件更新
---@param eventName any 事件名
---@param params any 传递参数
function ability:update( eventName, params )
    pool:update( eventName, params )
end

--注册任意事件
---@param event_name string 事件名: 
---@param func fun( event:event, params ) 回调函数 框架自带事件通常只有两个参数 1个event 一个table用于传参
---@return event
function ability:event_any( event_name, func )
    local params = {
        event = event_name,
        type = 'any',
    }
    --收到注册事件请求  注册事件
    eventpool:reg(
        event_name,
        params
    )

    --注册单位类型事件
    local event = pool:new( event_name )
    event.on_update = func
    return event
end

---@param eventName string 事件名:  单位-伤害  单位-受伤 详情查看module\unit\extends\abil\event.lua
---@param callback fun( event:event, params:table ) 回调函数 框架自带事件通常只有两个参数 1个event 一个table用于传参
---@return event|nil 如果事件不存在返回nil
function cAbility:event( eventName, callback )
    local params = {
        type = 'ability',
        ability = self,     --注册事件对象
    }
    --注册单位类型事件
    eventpool:reg( eventName, params )
    --指定对象事件 通过params获取返回的事件
    local event = params.ret
    if event then
        event.on_update = callback
    end
    return event
end
