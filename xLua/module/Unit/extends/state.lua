local GetUnitState            = cj.GetUnitState
local SetUnitState            = cj.SetUnitState
local ConvertUnitState        = cj.ConvertUnitState

local UNIT_STATE_LIFE         = cj.UNIT_STATE_LIFE
local UNIT_STATE_MAX_LIFE     = cj.UNIT_STATE_MAX_LIFE

local UNIT_STATE_MANA         = cj.UNIT_STATE_MANA
local UNIT_STATE_MAX_MANA     = cj.UNIT_STATE_MAX_MANA

local UNIT_STATE_ATTACK_TYPE  = ConvertUnitState(0x23)  --攻击类型
local UNIT_STATE_DEFENCE_TYPE = ConvertUnitState(0x50)  --护甲类型

local UNIT_STATE_DAMAGE_DICE = ConvertUnitState(0x10)  --骰子数量
local UNIT_STATE_DAMAGE_SIDE = ConvertUnitState(0x11)  --骰子面数
local UNIT_STATE_DAMAGE_BASE = ConvertUnitState(0x12)  --基础伤害
local UNIT_STATE_DAMAGE_BONUS = ConvertUnitState(0x13)  --附加伤害
local UNIT_STATE_DAMAGE_MIN = ConvertUnitState(0x14)  --最小攻击
local UNIT_STATE_DAMAGE_MAX = ConvertUnitState(0x15)  --最大攻击
local UNIT_STATE_DAMAGE_RANGE = ConvertUnitState(0x16)  --攻击距离

local UNIT_STATE_DAMAGE_COOL = ConvertUnitState(0x25)  --攻击间隔
local UNIT_STATE_DAMAGE_SPEED = ConvertUnitState(0x51)  --攻击速度

local UNIT_STATE_ARMOR = ConvertUnitState(0x20)  --护甲

xconst.unit.UNIT_STATE_LIFE = UNIT_STATE_LIFE
xconst.unit.UNIT_STATE_MAX_LIFE = UNIT_STATE_MAX_LIFE
xconst.unit.UNIT_STATE_MANA = UNIT_STATE_MANA
xconst.unit.UNIT_STATE_MAX_MANA = UNIT_STATE_MAX_MANA

xconst.unit.UNIT_STATE_ATTACK_TYPE = UNIT_STATE_ATTACK_TYPE
xconst.unit.UNIT_STATE_DEFENCE_TYPE = UNIT_STATE_DEFENCE_TYPE

xconst.unit.UNIT_STATE_DAMAGE_DICE = UNIT_STATE_DAMAGE_DICE
xconst.unit.UNIT_STATE_DAMAGE_SIDE = UNIT_STATE_DAMAGE_SIDE
xconst.unit.UNIT_STATE_DAMAGE_BASE = UNIT_STATE_DAMAGE_BASE
xconst.unit.UNIT_STATE_DAMAGE_BONUS = UNIT_STATE_DAMAGE_BONUS
xconst.unit.UNIT_STATE_DAMAGE_MIN = UNIT_STATE_DAMAGE_MIN
xconst.unit.UNIT_STATE_DAMAGE_MAX = UNIT_STATE_DAMAGE_MAX
xconst.unit.UNIT_STATE_DAMAGE_RANGE = UNIT_STATE_DAMAGE_RANGE

xconst.unit.UNIT_STATE_DAMAGE_COOL = UNIT_STATE_DAMAGE_COOL
xconst.unit.UNIT_STATE_DAMAGE_SPEED = UNIT_STATE_DAMAGE_SPEED

xconst.unit.UNIT_STATE_ARMOR = UNIT_STATE_ARMOR


local list = {
    ['生命值'] = {
        state =  UNIT_STATE_LIFE,
        get_japi = false,
        set_japi = false,
    },
    ['最大生命值'] = {
        state =  UNIT_STATE_MAX_LIFE,
        get_japi = false,
        set_japi = true,
    },
    ['法力值'] = {
        state =  UNIT_STATE_MANA,
        get_japi = false,
        set_japi = false,
    },
    ['最大法力值'] = {
        state =  UNIT_STATE_MAX_MANA,
        get_japi = false,
        set_japi = true,
    },
    ['攻击类型'] = {
        state =  UNIT_STATE_ATTACK_TYPE,
        get_japi = true,
        set_japi = true,
    },
    ['护甲类型'] = {
        state =  UNIT_STATE_DEFENCE_TYPE,
        get_japi = true,
        set_japi = true,
    },
}
---@class unit
local cUnit = unit.cUnit

---@ 获取生命值
---@return real
function cUnit:getHP(   )
    return GetUnitState(
        self.handle,
        UNIT_STATE_LIFE
    )
end

---@ 设置生命值
---@param value real
---@return real value原值返回
function cUnit:setHP( value )
    SetUnitState(
        self.handle,
        UNIT_STATE_LIFE,
        value
    )
    return value
end

---@ 获取法力值
---@return real
function cUnit:getMP(   )
    return GetUnitState(
        self.handle,
        UNIT_STATE_MANA
    )
end

---@ 设置法力值
---@param value real
---@return real value原值返回
function cUnit:setMP( value )
    SetUnitState(
        self.handle,
        UNIT_STATE_MANA,
        value
    )
    return value
end

----------------------------------------

---@ 获取生命值 百分比
---@return number 0.0 ~ 1.0
function cUnit:getHPprecent( )
    return GetUnitState(
        self.handle,
        UNIT_STATE_LIFE
    ) / GetUnitState(
        self.handle,
        UNIT_STATE_MAX_LIFE
    )
end

---@ 设置生命值 百分比 相对于最大生命值
---@param value number 0.0 ~ 1.0
---@return number 返回实际设置的生命值
function cUnit:setHPprecent( value )
    local realVal = GetUnitState(
        self.handle,
        UNIT_STATE_MAX_LIFE
    ) * value
    SetUnitState(
        self.handle,
        UNIT_STATE_LIFE,
        realVal
    )
    return realVal
end


---@ 获取法力值 百分比
---@return number 0.0 ~ 1.0
function cUnit:getMPprecent( )
    return GetUnitState(
        self.handle,
        UNIT_STATE_MANA
    ) / GetUnitState(
        self.handle,
        UNIT_STATE_MAX_MANA
    )
end

---@ 设置法力值 百分比 相对于最大法力值
---@param value number 0.0 ~ 1.0
---@return number 返回实际设置的法力值
function cUnit:setMPprecent( value )
    local realVal = GetUnitState(
        self.handle,
        UNIT_STATE_MAX_MANA
    ) * value
    SetUnitState(
        self.handle,
        UNIT_STATE_MANA,
        realVal
    )
    return realVal
end




-----------------------------------------
--               japi
----------------------------------------
local japiGetUnitState = japi.GetUnitState
local japiSetUnitState = japi.SetUnitState


---@ 获取护甲
---@return real
function cUnit:getArm( )
    return japiGetUnitState(
        self.handle,
        UNIT_STATE_ARMOR
    )
end

---@ 设置护甲
---@param value real
---@return real value原值返回
function cUnit:setArm( value )
    japiSetUnitState(
        self.handle,
        UNIT_STATE_ARMOR,
        value
    )
    return value
end

---@ 获取 攻击-最小值
---@return real
function cUnit:getDamMin( )
    return japiGetUnitState(
        self.handle,
        UNIT_STATE_DAMAGE_MIN
    )
end

---@ 设置 攻击-最小值
---@param value real
---@return real value原值返回
function cUnit:setDamMin( value )
    japiSetUnitState(
        self.handle,
        UNIT_STATE_DAMAGE_MIN,
        value
    )
    return value
end

---@ 获取 攻击-最大值
---@return real
function cUnit:getDamMax( )
    return japiGetUnitState(
        self.handle,
        UNIT_STATE_DAMAGE_MAX
    )
end

---@ 获取 单位攻击力(最大值)
---@return real
cUnit.getAtk = cUnit.getDamMax

---@ 设置 攻击-最大值
---@param value real
---@return real value原值返回
function cUnit:setDamMax( value )
    japiSetUnitState(
        self.handle,
        UNIT_STATE_DAMAGE_MAX,
        value
    )
    return value
end



---@ 获取属性
---@param cj_state handle cj.UNIT_STATE_LIFE
---@return real
function cUnit:getState( cj_state )
    return japiGetUnitState(
        self.handle,
        cj_state
    )
end

---@ 设置属性
---@param value real
---@return real value原值返回
function cUnit:setState( cj_state, value )
    japiSetUnitState(
        self.handle,
        cj_state,
        value
    )
    return value
end


return list