local UnitUseItem = cj.UnitUseItem
local UnitUseItemPoint = cj.UnitUseItemPoint
local UnitUseItemTarget = cj.UnitUseItemTarget
---@class item
local cItem = item.cItem
---@class unit
local cUnit = unit.cUnit

--- 创建物品给单位
---@param item_id string 支持 @ *
function cUnit:addItem( item_id )
    local it = item:new( { id = item_id } )
    it:give2unit( self )
    return it
end

--- 将物品给予单位
----
---@param u unit
function cItem:give2unit( u )
    cj.UnitAddItem( u.handle, self.handle )
end

--- 丢弃单位身上的 [指定物品]
----
---@param obj_item item
function cUnit:dropItem( obj_item )
    cj.UnitRemoveItem( self.handle, obj_item.handle )
end

--- 丢弃单位 [指定格子] 的物品
----
---@param slot_index int 物品栏格子号1-6
function cUnit:dropItemFromSlot( slot_index )
    cj.UnitRemoveItemFromSlot( self.handle, slot_index-1)
end

---@ 丢弃单位 [指定id] 的物品
---@param id string 物品id
---@param all bool true:全部丢  false(默认):只丢一个
function cUnit:dropItemById( id, all )
    for index = 0,5 do
        local it = cj.UnitItemInSlot( self.handle, index )  --可能是handle|int 不好做判断
        local hid = cj.GetHandleId( it )
        if hid ~= 0 then
            local obj_item =  item:h2o( hid )
            if obj_item.id == id then
                self:dropItem( obj_item )
                if not all then return end --只丢一个
            end
        end
    end
end

---@ 命令单位使用物品[无目标]
---@param obj_item item 要使用的物品
function cUnit:useItem( obj_item )
    UnitUseItem(self.handle, obj_item.handle)
end

---@ 命令单位使用物品[指定坐标]
---@param obj_item item 要使用的物品
---@param x real
---@param y real
function cUnit:useItemXY( obj_item, x, y )
    UnitUseItemPoint(self.handle, obj_item.handle, x, y)
end

---@ 命令单位使用物品[指定单位]
---@param obj_item item 要使用的物品
---@param obj_unit unit 目标
function cUnit:useItemUnit( obj_item, obj_unit )
    UnitUseItemTarget( self.handle, obj_item.handle, obj_unit.handle )
end

---对可破坏物使用的 等 destructable 写了再说