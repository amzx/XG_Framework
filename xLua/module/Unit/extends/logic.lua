---unit 扩展  逻辑类

local UnitAlive = require 'jass.ai'.UnitAlive
---@class unit
local cUnit = unit.cUnit

--- 是否存活
function cUnit:isAlive()
    return UnitAlive( self.handle )
end

--- 是否敌人
---@param target unit|player 可以单位或者玩家，反正原理都是判断玩家
---@return bool
function cUnit:isEnemy( target )
    return not self:isAlly( target )
end

--- 是否盟友
---@param target unit|player 可以单位或者玩家，反正原理都是判断玩家
---@return bool
function cUnit:isAlly( target )
    ---target 是单位
    if target.type == 'unit' then
        return self:getOwner():isAlly( target:getOwner() )
    end
    ---target 是玩家
    return self:getOwner():isAlly( target.handle )
end

--- 是否在区域中
---@param rct rect 判断的区域
---@return bool
function cUnit:isInRect(rct)
    if rct:isCoordInRect( self:getX(), self:getY() ) then
        return true
    end
    return false
end