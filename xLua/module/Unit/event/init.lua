--[[
    '单位-死亡',        '单位-伤害',
    '单位-击杀',        '单位-受伤',
    
    '单位-使用物品',
    '单位-获得物品',
    '单位-丢弃物品',
    '单位-抵押物品',

    '单位-出售物品',
    '单位-购买物品',

    '单位-出售单位',
    '单位-购买单位',
    '单位-被出售'

    ----------------后面的咕咕咕当中--------------

    '单位-学习技能',
    '单位-升级',

    '单位-发动技能',
]]
---@class unit
local cUnit = unit.cUnit
local pool = eventpool:new('任意单位事件') --新建事件池

---@private 通知事件更新
---@param eventName any 事件名
---@param params any 传递参数
function unit:update( eventName, params )
    pool:update( eventName, params )
end


-- 对[单位]自己注册事件
---@param self unit
---@param event_name string 事件名:  单位-伤害  单位-受伤 详情查看module\unit\evt.lua
---@param func fun( event:event, params:table ) 回调函数 框架自带事件通常只有两个参数 1个event 一个table用于传参
---@return event|nil 如果事件不存在返回nil
function cUnit:event( event_name, func )
    local params = {
        type = 'unit',
        unit = self,     --注册事件的单位
    }
    --注册单位类型事件
    eventpool:reg(
        event_name,    --通知指定的事件，有单位要注册事件
        params
    )
    --指定对象事件 通过params获取返回的事件
    local event = params.ret
    if event then
        event.on_update = func
    end
    return event
end


--注册[任意单位]事件
---@param event_name string 事件名: 单位-初始化 单位-销毁数据 单位-伤害  单位-受伤 详情查看module\unit\evt.lua
---@param func fun( event:event, params ) 回调函数 框架自带事件通常只有两个参数 1个event 一个table用于传参
---@return event
function unit:event_any( event_name, func )
    local params = {
        event = event_name,
        type = 'any',
    }
    --收到注册事件请求  注册事件
    eventpool:reg(
        event_name,
        params
    )

    --注册单位类型事件
    local event = pool:new( event_name )
    event.on_update = func
    return event
end

--注册[单位id]事件
---@param event_name string 事件名: 单位-初始化 单位-销毁数据 单位-伤害  单位-受伤 详情查看module\unit\evt.lua
---@param id string hpea
---@param func fun( event:event, params ) 回调函数 框架自带事件通常只有两个参数 1个event 一个table用于传参
---@return event|nil
function unit:event_id( event_name, id, func )
    local params = {
        event = event_name,
        type = 'id',
        id = id
    }
    --收到注册事件请求  注册事件
    eventpool:reg(
        event_name,
        params
    )

    --注册单位类型事件
    local event = params.ret
    if event then
        event.on_update = func
    end
    return event
end

--- 单位事件 垃圾收集
---@param event_name string 框架衍生的事件(单位-伤害 物品-被丢弃)不支持gc，只支持原生事件 例如 单位-受伤 单位-丢弃物品
function unit:event_gc( event_name )
    eventpool:reReg( event_name )
end

require 'xLua.module.Unit.event.death'
require 'xLua.module.Unit.event.damaged'

require 'xLua.module.Unit.event.useItem'
require 'xLua.module.Unit.event.pickItem'
require 'xLua.module.Unit.event.dropItem'
require 'xLua.module.Unit.event.pawnItem'
require 'xLua.module.Unit.event.buy&sellItem'
require 'xLua.module.Unit.event.buy&sellUnit'

require 'xLua.module.Unit.event.choose'