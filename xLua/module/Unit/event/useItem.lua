local group = require "module.Unit.extends.group"
--事件名 一般顺序都是 任意单位事件 -> id事件 -> 单位事件
-- 主动方事件 -> 被动方事件
local eventName_active = '单位-使用物品'    --主动方 事件名
local eventName_passive = '物品-被使用'   --被动方 事件名

--主要事件  也就是魔兽实际的事件
local mainEvent = eventName_active

local useItem = eventpool:new( eventName_active )
local itemUsed = eventpool:new( eventName_passive )

local switchAny = 0
local idListUnit = {}
local evtInit
local onInit

local cj = cj
local unit = unit
local GetManipulatingUnit = cj.GetManipulatingUnit
local GetManipulatedItem = cj.GetManipulatedItem
local TriggerRegisterUnitEvent = cj.TriggerRegisterUnitEvent
local CONST_UNIT_EVENT = cj.EVENT_UNIT_USE_ITEM

---------------------
--   触发事件分发
---------------------

local action = function ()
    local u = unit:h2o( GetManipulatingUnit() )
    local it = item:h2o( GetManipulatedItem() )

    local params = {

        unit =  u ,
        item =  it ,

    }

    unit:update( eventName_active, params ) --任意单位事件
    useItem:update( u.id, params )             --id事件
    useItem:update( u, params )

    
    item:update( eventName_passive, params )
    itemUsed:update( it.id, params )
    itemUsed:update( it, params )

end

local trg = trigger:new( action )

---------------------
--    注册与排泄
---------------------

---@param u unit
---@param Private_reReg bool 重新注册时跳过检测 强行注册
local function regEvent(u,Private_reReg)
    if Private_reReg then
        goto reReg
    end
    --检测是否已注册
    if u.events[mainEvent] then
        return
    end
    u.events[mainEvent] = true
    ::reReg::
    TriggerRegisterUnitEvent( trg.handle, u.handle, CONST_UNIT_EVENT )
end

--- 重新注册：排泄事件
local reReg = useItem:new( '重新注册事件' )
reReg.on_update = function (event, params)
    trg:del()   --删除触发器重新创建：排泄所有事件
    trg = trigger:new(action)
    --重新为单位注册事件
    group:enum(function (u)
        if u.events[mainEvent] then
            regEvent( u, true )
        end
    end)
end

---------------------
--    初始化事件
---------------------

local enable
enable = function ()
    evtInit = unit:event_any( '单位-初始化', onInit )
    evtInit.on_update = onInit
    enable = function () end
    --注册已存在的所有单位
    group:enum(function (u)
        regEvent( u )
    end)
end
onInit = function (event, params)

    --2仅 id事件
    if switchAny == 2 then
        if not idListUnit[params.unit.id] then
            return
        end
    end
    local u = params.unit
    regEvent( u )

end

---------------------
--      接收器
---------------------

--收到注册：单位-使用物品
local reg_unit = useItem:new( '注册事件' )
reg_unit.on_update = function ( event, params )
    local type = params.type
    if type == 'unit' then
        local u = params.unit
        --注册事件
        regEvent( u )
        --返回一个事件
        params.ret = useItem:new( u )
        --记录进单位对象中。单位销毁时方便销毁事件
        table_insert(u.events, params.ret)
    elseif type == 'any' then
        --注册id类别和任意单位时启用初始化事件捕捉
        switchAny = 1
        enable()
    else--if type == 'id' then
        if switchAny == 0 then
            switchAny = 2
        end
        enable()
        idListUnit[params.id] = true
        params.ret = useItem:new( params.id )
    end

end



--收到注册：物品-被丢弃
local reg_item = itemUsed:new( '注册事件' )
reg_item.on_update = function (event, params )
    local type = params.type
    
    if type == 'item' then
        local it = params.item
        --返回一个事件
        params.ret = itemUsed:new( it )
        table_insert(it.events, params.ret)
    elseif type == 'id' then
        params.ret = itemUsed:new( params.id )
    end

    --要捕捉物品被丢弃就得给所有单位上丢弃事件
    switchAny = 1
    enable()
    
end


