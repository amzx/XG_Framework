local group = require "module.Unit.extends.group"
--事件名 一般顺序都是 任意单位事件 -> id事件 -> 单位事件
--主动方事件 -> 被动方事件
local eventName_active = '单位-伤害'    --主动方 事件名
local eventName_passive = '单位-受伤'   --被动方 事件名

--主要事件  也就是魔兽实际的事件s
local mainEvent = eventName_passive

local hurt = eventpool:new( eventName_active )
local gethurt = eventpool:new( eventName_passive )


local evtInit
local onInit

local cj = cj
local unit = unit
local GetEventDamageSource = cj.GetEventDamageSource
local GetTriggerUnit = cj.GetTriggerUnit
local GetEventDamage = cj.GetEventDamage
local EXSetEventDamage = japi.EXSetEventDamage
local TriggerRegisterUnitEvent = cj.TriggerRegisterUnitEvent
local CONST_UNIT_EVENT = cj.EVENT_UNIT_DAMAGED

---------------------
--   触发事件分发
---------------------

local action = function ()
    local source = unit:h2o( GetEventDamageSource() )
    local target = unit:h2o( GetTriggerUnit() )
    local damage = GetEventDamage()
    local params = {
        source = source,
        target = target,
        damage = damage,
    }

    unit:update( eventName_active, params ) --任意单位事件
    hurt:update( source.id, params )             --id事件
    hurt:update( source, params )

    unit:update( eventName_passive, params ) --任意单位事件
    gethurt:update( target.id, params )             --id事件
    gethurt:update( target, params )

    --- 判断是否有修改伤害
    if type(params.damage) ~= 'number' then
        return --伤害已经被修改得不成样了
    end
    --- 伤害值仍是数值 判断是否被改动
    if params.damage == damage then
        return  --伤害值没有变动
    end
    --- 伤害已被修改
    EXSetEventDamage( params.damage )

end

local trg = trigger:new( action )

---------------------
--    注册与排泄
---------------------

---@param u unit
---@param Private_reReg bool 重新注册时跳过检测 强行注册
local function regEvent(u,Private_reReg)
    if Private_reReg then
        goto reReg
    end
    --检测是否已注册
    if u.events[mainEvent] then
        return
    end
    u.events[mainEvent] = true
    ::reReg::
    TriggerRegisterUnitEvent( trg.handle, u.handle, CONST_UNIT_EVENT )
end

--- 重新注册：排泄事件
local reReg = gethurt:new( '重新注册事件' )
reReg.on_update = function (event, params)
    trg:del()   --删除触发器重新创建：排泄所有事件
    trg = trigger:new(action)
    --重新为单位注册事件
    group:enum(function (u)
        if u.events[mainEvent] then
            regEvent( u, true )
        end
    end)
end

---------------------
--    初始化事件
---------------------

---- 单位死亡事件需要特殊处理 因为默认每个单位都要注册 不管作者有没有注册死亡事件
--所以会和其他事件有点不一样
local enable
enable = function ()
    evtInit = unit:event_any( '单位-初始化', onInit )
    evtInit.on_update = onInit
    --注册已存在的所有单位
    group:enum(function (u)
        regEvent( u )
    end)
end
onInit = function (event, params)
    --单位初始化时注册事件
    local u = params.unit
    regEvent( u )

end
--默认开启单位初始化注册
enable()

---------------------
--      接收器
---------------------

--收到注册：单位-受伤
local reg_gethurt = gethurt:new( '注册事件' )
reg_gethurt.on_update = function ( event, params )
    local type = params.type
    if type == 'unit' then
        local u = params.unit
        --返回一个事件
        params.ret = gethurt:new( u )
        --记录进单位对象中。单位销毁时方便销毁事件
        table_insert(u.events, params.ret)
    elseif type == 'id' then
        params.ret = gethurt:new( params.id )
    end

end

--收到注册：单位-伤害
local reg_hurt = hurt:new( '注册事件' )
reg_hurt.on_update = function (event, params )
    local type = params.type
    if type == 'unit' then
        local u = params.unit
        --返回一个事件
        params.ret = hurt:new( u )
        table_insert(u.events, params.ret)
    elseif type == 'id' then
        params.ret = hurt:new( params.id )
    end
end

