local group = require "module.Unit.extends.group"
--事件名 一般顺序都是 任意单位事件 -> id事件 -> 单位事件
-- 主动方事件 -> 被动方事件
local eventName_active = '玩家-选择单位'    --主动方 事件名
local eventName_passive = '单位-被选择'   --被动方 事件名

--主要事件  也就是魔兽实际的事件
local mainEvent = eventName_active

local selectUnit = eventpool:new( eventName_active )
local unitSelected = eventpool:new( eventName_passive )

local switchAny = 0
local idListUnit = {}
local evtInit
local onInit

local cj = cj
local unit = unit
local GetTriggerPlayer = cj.GetTriggerPlayer
local GetTriggerUnit = cj.GetTriggerUnit
local TriggerRegisterPlayerUnitEvent = cj.TriggerRegisterPlayerUnitEvent
local CONST_UNIT_EVENT = cj.EVENT_PLAYER_UNIT_SELECTED

---------------------
--   触发事件分发
---------------------

local action = function ()
    local p = player:h2o( GetTriggerPlayer() )
    local u = unit:h2o( GetTriggerUnit() )

    local params = {

        player =  p ,
        unit =  u ,

    }

    player:update( eventName_active, params ) --任意单位事件
    selectUnit:update( p.id, params )             --id事件
    selectUnit:update( p, params )

    
    unit:update( eventName_passive, params )
    unitSelected:update( u.id, params )
    unitSelected:update( u, params )

end

local trg = trigger:new( action )

---------------------
--    注册与排泄
---------------------

---@param p player
---@param Private_reReg bool 重新注册时跳过检测 强行注册
local function regEvent(p,Private_reReg)
    if Private_reReg then
        goto reReg
    end
    --检测是否已注册
    if p.events[mainEvent] then
        return
    end
    p.events[mainEvent] = true
    ::reReg::
    TriggerRegisterPlayerUnitEvent( trg.handle, p.handle, CONST_UNIT_EVENT, nil )
end

--- 重新注册：排泄事件
local reReg = selectUnit:new( '重新注册事件' )
reReg.on_update = function (event, params)
    trg:del()   --删除触发器重新创建：排泄所有事件
    trg = trigger:new(action)

    force:enum(function (p)
        if p.events[mainEvent] then
            regEvent( p, true )
        end
    end)
end

---------------------
--    初始化事件
---------------------

local enable
enable = function ()
    enable = function () end
    --注册所有玩家
    force:enum(function (p)
        regEvent( p )
    end)
end


---------------------
--      接收器
---------------------

--收到注册：玩家-选择单位
local reg_player = selectUnit:new( '注册事件' )
reg_player.on_update = function ( event, params )
    local type = params.type
    if type == 'player' then
        local p = params.player
        --注册事件
        regEvent( p )
        --返回一个事件
        params.ret = selectUnit:new( p )
        table_insert(p.events, params.ret)
    else--if type == 'any' then
        --注册id类别和任意单位时启用初始化事件捕捉
        switchAny = 1
        enable()
    end
end



--收到注册：单位-被选择
local reg_unit = unitSelected:new( '注册事件' )
reg_unit.on_update = function (event, params )
    local type = params.type

    if type == 'unit' then
        local u = params.unit
        --返回一个事件
        params.ret = unitSelected:new( u )
        table_insert(u.events, params.ret)
    elseif type == 'id' then
        if switchAny == 0 then
            switchAny = 2
        end
        enable()
        --idListUnit[params.id] = true
        params.ret = unitSelected:new( params.id )
    end

    --要捕捉单位被选取就得给所有玩家注册选择事件
    switchAny = 1
    enable()
    
end


