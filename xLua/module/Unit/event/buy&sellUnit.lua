local group = require "module.Unit.extends.group"
--事件名 一般顺序都是 任意单位事件 -> id事件 -> 单位事件
-- 主动方事件 -> 被动方事件
local eventName_active = '单位-出售单位'    --主动方 事件名
local eventName_active2 = '单位-购买单位'    --主动方 事件名
local eventName_passive = '单位-被出售'   --被动方 事件名

--主要事件  也就是魔兽实际的事件
local mainEvent = eventName_active

local sellUnit = eventpool:new( eventName_active )
local buyUnit = eventpool:new( eventName_active2 )
local unitSold = eventpool:new( eventName_passive )

local switchAny = 0
local idListUnit = {}
local evtInit
local onInit

local cj = cj
local unit = unit
local GetBuyingUnit = cj.GetBuyingUnit
local GetSellingUnit = cj.GetSellingUnit
local GetSoldUnit = cj.GetSoldUnit
local TriggerRegisterUnitEvent = cj.TriggerRegisterUnitEvent
local CONST_UNIT_EVENT = cj.EVENT_UNIT_SELL

---------------------
--   触发事件分发
---------------------

local action = function ()
    local seller = unit:h2o( GetSellingUnit() )
    local buyer = unit:h2o( GetBuyingUnit() )
    local u = unit:h2o( GetSoldUnit() )

    local params = {

        seller =  seller ,
        buyer = buyer ,
        unit =  u ,
        sold = u ,

    }

    unit:update( eventName_active, params ) --任意单位事件
    sellUnit:update( seller.id, params )             --id事件
    sellUnit:update( seller, params )

    unit:update( eventName_active2, params )
    buyUnit:update( buyer.id, params )
    buyUnit:update( buyer, params )

    unit:update( eventName_passive, params )
    unitSold:update( u.id, params )
    -- unitSold:update( it, params )

end

local trg = trigger:new( action )

---------------------
--    注册与排泄
---------------------

---@param u unit
---@param Private_reReg bool 重新注册时跳过检测 强行注册
local function regEvent(u,Private_reReg)
    if Private_reReg then
        goto reReg
    end
    --检测是否已注册
    if u.events[mainEvent] then
        return
    end
    u.events[mainEvent] = true
    ::reReg::
    TriggerRegisterUnitEvent( trg.handle, u.handle, CONST_UNIT_EVENT )
end

--- 重新注册：排泄事件
local reReg = sellUnit:new( '重新注册事件' )
reReg.on_update = function (event, params)
    trg:del()   --删除触发器重新创建：排泄所有事件
    trg = trigger:new(action)
    --重新为单位注册事件
    group:enum(function (u)
        if u.events[mainEvent] then
            regEvent( u, true )
        end
    end)
end

---------------------
--    初始化事件
---------------------

local enable
enable = function ()
    evtInit = unit:event_any( '单位-初始化', onInit )
    evtInit.on_update = onInit
    enable = function () end
    --注册已存在的所有单位
    group:enum(function (u)
        regEvent( u )
    end)
end
onInit = function (event, params)

    --2仅 id事件
    if switchAny == 2 then
        if not idListUnit[params.unit.id] then
            return
        end
    end
    local u = params.unit
    regEvent( u )

end

---------------------
--      接收器
---------------------

--收到注册：单位-出售单位
local reg_unit = sellUnit:new( '注册事件' )
reg_unit.on_update = function ( event, params )
    local type = params.type
    if type == 'unit' then
        local u = params.unit
        --注册事件
        regEvent( u )
        --返回一个事件
        params.ret = sellUnit:new( u )
        --记录进单位对象中。单位销毁时方便销毁事件
        table_insert(u.events, params.ret)
    elseif type == 'any' then
        --注册id类别和任意单位时启用初始化事件捕捉
        switchAny = 1
        enable()
    else--if type == 'id' then
        if switchAny == 0 then
            switchAny = 2
        end
        enable()
        idListUnit[params.id] = true
        params.ret = sellUnit:new( params.id )
    end

end


--收到注册：单位-购买单位
local reg_unit = buyUnit:new( '注册事件' )
reg_unit.on_update = function ( event, params )
    local type = params.type
    if type == 'unit' then
        local u = params.unit
        --注册事件
        regEvent( u )
        --返回一个事件
        params.ret = buyUnit:new( u )
        --记录进单位对象中。单位销毁时方便销毁事件
        table_insert(u.events, params.ret)
    elseif type == 'any' then
        --注册id类别和任意单位时启用初始化事件捕捉
        switchAny = 1
        enable()
    else--if type == 'id' then
        if switchAny == 0 then
            switchAny = 2
        end
        enable()
        idListUnit[params.id] = true
        params.ret = buyUnit:new( params.id )
    end

end


--收到注册：单位-被出售
local reg_unit = unitSold:new( '注册事件' )
reg_unit.on_update = function ( event, params )
    local type = params.type
    if type == 'unit' then
        local u = params.unit
        --注册事件
        regEvent( u )
        --返回一个事件
        params.ret = unitSold:new( u )
        --记录进单位对象中。单位销毁时方便销毁事件
        table_insert(u.events, params.ret)
    elseif type == 'any' then
        --注册id类别和任意单位时启用初始化事件捕捉
        switchAny = 1
        enable()
    else--if type == 'id' then
        if switchAny == 0 then
            switchAny = 2
        end
        enable()
        idListUnit[params.id] = true
        params.ret = unitSold:new( params.id )
    end

end

