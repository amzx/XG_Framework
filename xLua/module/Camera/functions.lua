
local function setmetatableindex(t, index)
    local mt = getmetatable(t)
    if not mt then mt = {} end
    if not mt.__index then
        mt.__index = index
        setmetatable(t, mt)
    elseif mt.__index ~= index then
        setmetatableindex(mt, index)
    end
end

function Class(classname, ...)
    local cls = {__cname = classname}

    local supers = {...}
    for _, super in ipairs(supers) do
        local superType = type(super)
        assert(superType == "nil" or superType == "table" or superType ==
                   "function", string.format(
                   "class() - create class \"%s\" with invalid super class type \"%s\"",
                   classname, superType))

        if superType == "function" then
            assert(cls.__create == nil, string.format(
                       "class() - create class \"%s\" with more than one creating function",
                       classname));
            -- if super is function, set it to __create
            cls.__create = super
        elseif superType == "table" then
            -- super is pure lua class
            cls.__supers = cls.__supers or {}
            cls.__supers[#cls.__supers + 1] = super
            if not cls.super then
                -- set first super pure lua class as class.super
                cls.super = super
            end
        else
            error(string.format(
                      "class() - create class \"%s\" with invalid super type",
                      classname), 0)
        end
    end

    cls.__index = cls
    if not cls.__supers or #cls.__supers == 1 then
        setmetatable(cls, {__index = cls.super})
    else
        setmetatable(cls, {
            __index = function(_, key)
                local supers = cls.__supers
                for i = 1, #supers do
                    local super = supers[i]
                    if super[key] then return super[key] end
                end
            end
        })
    end

    if not cls.ctor then
        -- add default constructor
        cls.ctor = function() end
    end
    cls._new = function(...)
        local instance
        if cls.__create then
            instance = cls.__create(...)
        else
            instance = {}
        end
        setmetatableindex(instance, cls)
        instance.class = cls
        instance:ctor(...)
        return instance
    end
    cls.new = cls._new
    cls.create = function(_, ...) return cls._new(...) end

    return cls
end


local function iskindof_(cls, name)
    local __index = rawget(cls, "__index")
    if type(__index) == "table" and rawget(__index, "__cname") == name then
        return true
    end

    local __supers = rawget(__index, "__supers")
    if not __supers then return false end
    for _, super in ipairs(__supers) do
        if iskindof_(super, name) then return true end
    end
    return false
end

function IsKindOf(obj, classname)
    local t = type(obj)
    if t ~= "table" then return false end

    local mt
    mt = getmetatable(obj)
    if mt then return iskindof_(mt, classname) end
    return false
end

local function checknumber(value, base) return tonumber(value, base) or 0 end

function math.round(value)
    value = checknumber(value)
    return math.floor(value + 0.5)
end
