ability = require 'xLua.module.Unit.extends.abil'    --技能扩展

require 'xLua.module.Unit.extends.state'   --获取属性扩展

require 'xLua.module.Unit.extends.item'   --物品扩展

require 'xLua.module.Unit.extends.group'   --单位组扩展

require 'xLua.module.Unit.extends.logic'   --逻辑类