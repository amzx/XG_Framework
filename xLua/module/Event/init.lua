
--- Ex 发送注册通知
---@param evenNmae string 你需要通知的事件名称
---@param params table
function eventpool:reg( evenNmae, params )

    eventpool:notify( evenNmae, '注册事件', params )
    
end

--- Ex 发送重新注册通知 [排泄]
---@param evenNmae string 你需要通知的事件名称
---@param params table 没用。备用
function eventpool:reReg( evenNmae, params )

    eventpool:notify( evenNmae, '重新注册事件', params )
    
end


--玩家事件
require 'xLua.module.Player.event'
--单位事件
require 'xLua.module.unit.event'
--物品事件
require 'xLua.module.item.event'
--可破坏物事件
require 'xLua.module.Destructable.events'