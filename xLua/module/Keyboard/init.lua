local japi = require 'jass.japi'

if japi.DzIsChatBoxOpen then
    return require 'xLua.module.Keyboard.kk'
end

require 'Message' --为了兼容kk，需要在非kk环境时手动加载
return require 'xLua.module.Keyboard.default'
