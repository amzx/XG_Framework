local mKeyboard = {}
local const = require 'xLua.module.Keyboard.constant.kk'
xconst.keyboard = const

local press = eventpool:new( '键盘-按下' )
local release = eventpool:new( '键盘-松开' )

local map = {
    key_up = '键盘-松开',
    key_down = '键盘-按下',
}

local map_event = { }
local isKeyDown = {}

local japi = japi

local DzTriggerRegisterKeyEventByCode = japi.DzTriggerRegisterKeyEventByCode
local DzGetTriggerKey = japi.DzGetTriggerKey
local DzIsChatBoxOpen = japi.DzIsChatBoxOpen
--[[
call DzTriggerRegisterKeyEventByCode( null, 9, 0, false, function Trig_11Func002KT)

['KEY_触发按键'] = //DzGetTriggerKey(),
['KEY_鼠标左键'] = //1,
['KEY_鼠标右键'] = //2,
]]

local notify_key_down = function ( key )
    isKeyDown[key] = true
end
local notify_key_up = function ( key )
    isKeyDown[key] = false
end

local k_KEY_ALT = const.KEY_ALT
local k_COMB_ALT = const.COMB_ALT
local k_KEY_CTRL = const.KEY_CTRL
local k_COMB_CTRL = const.COMB_CTRL
local k_KEY_SHIFT = const.KEY_SHIFT
local k_COMB_SHIFT = const.COMB_SHIFT

--按下的键
local getCombKey = function ( key )
    local ret = 0

    if isKeyDown[k_KEY_ALT] then
        ret = ret + k_COMB_ALT
    end
    if isKeyDown[k_KEY_SHIFT] then
        ret = ret + k_COMB_SHIFT
    end
    if isKeyDown[k_KEY_CTRL] then
        ret = ret + k_COMB_CTRL
    end

    return ret
end

local genEvtName = function (comb, key)
    return comb .. '+' .. key
end

local key_up = function ()
    local key = DzGetTriggerKey() -- 整数 键码

    notify_key_up(key)

    local comb = getCombKey() -- 组合键 默认为0

    local eventName = genEvtName( comb, key )

    local params = {
        combKey = comb,
        key = key,
    }
    release:update(eventName, params)

    --修改 msg.code 可以进行改键
    --msg.state = params.combKey
    --msg.code = params.key

    --return key_up(msg) and not params.ret
end

local key_down = function ()
    if DzIsChatBoxOpen() then
        return
    end

    local key = DzGetTriggerKey() -- 整数 键码

    if isKeyDown[key] then
        return
    end

    local comb = getCombKey() -- 组合键 默认为0

    notify_key_down(key)

    local eventName = genEvtName( comb, key )

    local params = {
        combKey = comb,
        key = key,
    }
    press:update(eventName, params)

    --修改 msg.code 可以进行改键
    --msg.state = params.combKey
    --msg.code = params.key

    --return not params.ret
end

local regKey = function ( key )
    if map_event[key] then
        return
    end
    map_event[key] = true
    DzTriggerRegisterKeyEventByCode( nil, key, 0, false,  key_up )
    DzTriggerRegisterKeyEventByCode( nil, key, 1, false,  key_down )
end

local regCombKey = function ( combKey )
    --使用位运算判断combKey中是否包含 k_COMB_SHIFT， k_COMB_ALT, k_COMB_CTRL
    if combKey & k_COMB_SHIFT ~= 0 then
        regKey( k_KEY_SHIFT )
    end
    if combKey & k_COMB_ALT ~= 0 then
        regKey( k_KEY_ALT )
    end
    if combKey & k_COMB_CTRL ~= 0 then
        regKey( k_KEY_CTRL )
    end
end

regKey( k_KEY_SHIFT )
regKey( k_KEY_ALT )
regKey( k_KEY_CTRL )

--regCombKey( combKey )

--- [异步]注册热键
---@param eventNmae string 事件： 键盘-按下/key_down  键盘-松开/key_up
---@param combKey int 组合键，可以多个组合键相加 xconst.keyboard.COMB_
---@param key int 主要按键 xconst.keyboard.KEY_
---@param callback fun(event:event,params:table) 设置 params.ret = true 时拦截事件
---@return event
function mKeyboard:hotkey( eventNmae, combKey, key, callback )
    local params = {
        type = 'keyboard',
        combKey = combKey,
        key = key,
    }
    regKey( key )
    eventpool:reg( map[eventNmae] or eventNmae, params )
    ---@type event
    local event = params.ret
    if event then
        event.on_update = callback
    end
    return event
end


--------------------------------
--          注册器
--------------------------------

local regPress = press:new('注册事件')
regPress.on_update = function (event, params)
    local comb = params.combKey
    local key = params.key
    local eventName = genEvtName( comb, key )
    params.ret = press:new( eventName )
end

local regRelease = release:new('注册事件')
regRelease.on_update = function (event, params)
    local comb = params.combKey
    local key = params.key
    local eventName = genEvtName( comb, key )
    params.ret = release:new( eventName )
end

return mKeyboard