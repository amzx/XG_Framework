return {

    ['KEY_0'] = 48,
    ['KEY_1'] = 49,
    ['KEY_2'] = 50,
    ['KEY_3'] = 51,
    ['KEY_4'] = 52,
    ['KEY_5'] = 53,
    ['KEY_6'] = 54,
    ['KEY_7'] = 55,
    ['KEY_8'] = 56,
    ['KEY_9'] = 57,

    ['KEY_A'] = 65,
    ['KEY_B'] = 66,
    ['KEY_C'] = 67,
    ['KEY_D'] = 68,
    ['KEY_E'] = 69,
    ['KEY_F'] = 70,
    ['KEY_G'] = 71,
    ['KEY_H'] = 72,
    ['KEY_I'] = 73,
    ['KEY_J'] = 74,
    ['KEY_K'] = 75,
    ['KEY_L'] = 76,
    ['KEY_M'] = 77,
    ['KEY_N'] = 78,
    ['KEY_O'] = 79,
    ['KEY_P'] = 80,
    ['KEY_Q'] = 81,
    ['KEY_R'] = 82,
    ['KEY_S'] = 83,
    ['KEY_T'] = 84,
    ['KEY_U'] = 85,
    ['KEY_V'] = 86,
    ['KEY_W'] = 87,
    ['KEY_X'] = 88,
    ['KEY_Y'] = 89,
    ['KEY_Z'] = 90,

    ['KEY_Win左'] = 91,
    ['KEY_Win右'] = 92,
    ['KEY_Fn'] = 93,

    --小键盘
    ['KEY_NUM_0'] = 96,
    ['KEY_NUM_1'] = 97,
    ['KEY_NUM_2'] = 98,
    ['KEY_NUM_3'] = 99,
    ['KEY_NUM_4'] = 100,
    ['KEY_NUM_5'] = 101,
    ['KEY_NUM_6'] = 102,
    ['KEY_NUM_7'] = 103,
    ['KEY_NUM_8'] = 104,
    ['KEY_NUM_9'] = 105,
    ['KEY_*'] = 106,
    ['KEY_+'] = 107,
    ['KEY_-'] = 109,
    ['KEY_.'] = 110,
    ['KEY_/'] = 111,
    -- 


    ['KEY_F1'] = 112,
    ['KEY_F2'] = 113,
    ['KEY_F3'] = 114,
    ['KEY_F4'] = 115,
    ['KEY_F5'] = 116,
    ['KEY_F6'] = 117,
    ['KEY_F7'] = 118,
    ['KEY_F8'] = 119,
    ['KEY_F9'] = 120,
    ['KEY_F10'] = 121,
    ['KEY_F11'] = 122,
    ['KEY_F12'] = 123,

    --  键盘锁
    ['KEY_CAPS'] = 20,  --字母大小写锁
    ['KEY_NUM'] = 144, -- 小键盘锁
    ['KEY_SCLK'] = 145, --SCLK键盘灯




    --字母数字右边的一堆符号键

    -- 暂无替代
    --['KEY_\\'] = 276,
    --['KEY_['] = 274,
    --['KEY_]'] = 275,
    --KEY_PABK = 529, --PAuse BreaK

    ['KEY_ESC'] = 27,
    ['KEY_SPACE'] = 32,

    --方向键
    ['KEY_LEFT'] = 37, -- ←
    ['KEY_UP'] = 38, -- ↑
    ['KEY_RIGHT'] = 39, -- →
    ['KEY_DOWN'] = 40, -- ↓


    --控制键
    ['KEY_PGUP'] = 33, -- PageUp
    ['KEY_PGDN'] = 34, -- PageDown

    ['KEY_HOME'] = 36,
    ['KEY_END'] = 35,

    ['KEY_INS'] = 45,
    ['KEY_DEL'] = 46,

    -- **** dz特有 ****

    --主键盘区域
    ['KEY_;'] = 186, -- ; :
    ['KEY_='] = 187,
    ['KEY_<'] = 188,
    ['KEY__'] = 189,
    ['KEY_>'] = 190,
    ['KEY_?'] = 191,
    ['KEY_`'] = 192, -- ` ~
    ['KEY_\''] = 222, -- ' "

    ['KEY_Pause'] = 19,
    ['KEY_PrintScreen'] = 44,

    -- ****************

    ['KEY_BACKSPACE'] = 8,
    ['KEY_TAB'] = 9,
    ['KEY_ENTER'] = 13,

    --功能键
    ['KEY_SHIFT'] = 16,
    ['KEY_CTRL'] = 17,
    ['KEY_ALT'] = 18,

    --KEY_SHIFT = 0,
    --KEY_CTRL = 1,
    --KEY_ALT = 2,

    --组合键状态 多个功能键相加

    COMB_NONE = 0,
    COMB_SHIFT = 1,
    COMB_CTRL = 2,
    COMB_ALT = 4,

}
