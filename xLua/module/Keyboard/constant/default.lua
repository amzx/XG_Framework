return {
    KEY_A = 65,
    KEY_B = 66,
    KEY_C = 67,
    KEY_D = 68,
    KEY_E = 69,
    KEY_F = 70,
    KEY_G = 71,
    KEY_H = 72,
    KEY_I = 73,
    KEY_J = 74,
    KEY_K = 75,
    KEY_L = 76,
    KEY_M = 77,
    KEY_N = 78,
    KEY_O = 79,
    KEY_P = 80,
    KEY_Q = 81,
    KEY_R = 82,
    KEY_S = 83,
    KEY_T = 84,
    KEY_U = 85,
    KEY_V = 86,
    KEY_W = 87,
    KEY_X = 88,
    KEY_Y = 89,
    KEY_Z = 90,

    KEY_0 = 48,
    KEY_1 = 49,
    KEY_2 = 50,
    KEY_3 = 51,
    KEY_4 = 52,
    KEY_5 = 53,
    KEY_6 = 54,
    KEY_7 = 55,
    KEY_8 = 56,
    KEY_9 = 57,

    ['KEY_`'] = 256, -- ` ~

    --小键盘

    KEY_NUM_0 = 257,
    KEY_NUM_1 = 258,
    KEY_NUM_2 = 259,
    KEY_NUM_3 = 260,
    KEY_NUM_4 = 261,
    KEY_NUM_5 = 262,
    KEY_NUM_6 = 263,
    KEY_NUM_7 = 264,
    KEY_NUM_8 = 265,
    KEY_NUM_9 = 266,

    ['KEY_+'] = 267,
    ['KEY_-'] = 268,
    ['KEY_*'] = 269,
    ['KEY_/'] = 270,
    ['KEY_.'] = 271, --小键盘的点
    
    --字母数字右边的一堆符号键

    ['KEY_='] = 272,
    ['KEY__'] = 273, --大键盘区域 = 旁边的的减号
    ['KEY_['] = 274,
    ['KEY_]'] = 275,
    ['KEY_\\'] = 276,
    ['KEY_;'] = 277,  -- ; :
    ['KEY_\''] = 278, -- ' "
    ['KEY_<'] = 279,  -- , <
    ['KEY_>'] = 280,  -- . >
    ['KEY_?'] = 281,  -- / ?

    
    KEY_SPACE = 32,
    KEY_ESC = 512,
    KEY_ENTER = 513, --大键盘和小键盘区域都是相同值
    KEY_BACKSPACE = 514, --退格键
    KEY_TAB = 515,

    --方向键

    KEY_LEFT = 516, -- ←
    KEY_UP = 517, -- ↑
    KEY_RIGHT = 518, -- →
    KEY_DOWN = 519, -- ↓

    --控制键

    KEY_INS = 520,
    KEY_DEL = 521,
    KEY_HOME = 522,
    KEY_END = 523,
    KEY_PGUP = 524,
    KEY_PGDN = 525,

    --键盘灯

    KEY_CAPS = 526, --大小写切换键
    KEY_NUM = 527, --小键盘开关
    KEY_SCLK = 528, --SCLK键盘灯

    KEY_PABK = 529, --PAuse BreaK

    KEY_F1 = 768,
    KEY_F2 = 769,
    KEY_F3 = 770,
    KEY_F4 = 771,
    KEY_F5 = 772,
    KEY_F6 = 773,
    KEY_F7 = 774,
    KEY_F8 = 775,
    KEY_F9 = 776,
    KEY_F10 = 777,
    KEY_F11 = 778,
    KEY_F12 = 779,

    --功能键

    KEY_SHIFT = 0,
    KEY_CTRL = 1,
    KEY_ALT = 2,

    --组合键状态 多个功能键相加

    COMB_NONE = 0,
    COMB_SHIFT = 1,
    COMB_CTRL = 2,
    COMB_ALT = 4,

}
