--[[
    '玩家-按下ESC',
    '玩家-离开游戏',
    '玩家-聊天消息',

    '玩家-选择单位',
]]
---@class cPlayer
local cPlayer = player.cPlayer
local pool = eventpool:new('任意玩家事件') --新建事件池

---@private 通知事件更新
---@param eventName any 事件名
---@param params any 传递参数
function player:update( eventName, params )
    pool:update( eventName, params )
end


-- 对[玩家]注册事件
---@param event_name string 事件名:  玩家-按下ESC  玩家-聊天信息 玩家-离开游戏 详情查看module\player\event
---@param func fun( event:event, params:table ) 回调函数 框架自带事件通常只有两个参数 1个event 一个table用于传参
---@return event|nil 如果事件不存在返回nil
function cPlayer:event( event_name, func )
    local params = {
        type = 'player',
        player = self,     --注册事件的单位
    }
    --注册单位类型事件
    eventpool:reg(
        event_name,    --通知指定的事件，有单位要注册事件
        params
    )
    --指定对象事件 通过params获取返回的事件
    local event = params.ret
    if event then
        event.on_update = func
    end
    return event
end


--注册[任意玩家]事件
---@param event_name string 事件名: 玩家-初始化 玩家-按下ESC  玩家-聊天信息 玩家-离开游戏 详情查看module\player\event
---@param func fun( event:event, params ) 回调函数 框架自带事件通常只有两个参数 1个event 一个table用于传参
---@return event
function player:event_any( event_name, func )
    local params = {
        event = event_name,
        type = 'any',
    }
    --收到注册事件请求  注册事件
    eventpool:reg(
        event_name,
        params
    )

    --注册单位类型事件
    local event = pool:new( event_name )
    event.on_update = func
    return event
end


--- 单位事件 垃圾收集
---@param event_name string 框架衍生的事件(单位-伤害 物品-被丢弃)不支持gc，只支持原生事件 例如 单位-受伤 单位-丢弃物品
function player:event_gc( event_name )
    eventpool:reReg( event_name )
end

require 'xLua.module.Player.event.pressESC'
require 'xLua.module.Player.event.leave'
require 'xLua.module.Player.event.chat'