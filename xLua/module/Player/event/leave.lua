--事件名 一般顺序都是 任意单位事件 -> id事件 -> 单位事件
-- 主动方事件 -> 被动方事件
local eventName_active = '玩家-离开游戏'    --主动方 事件名
--local eventName_passive = ' '   --被动方 事件名

--主要事件  也就是魔兽实际的事件
local mainEvent = eventName_active

local leave = eventpool:new( eventName_active )
--local itemDiscarded = eventpool:new( eventName_passive )

local evtInit
local onInit

local cj = cj
local player = player
local GetTriggerPlayer = cj.GetTriggerPlayer
local TriggerRegisterPlayerEvent = cj.TriggerRegisterPlayerEvent
local CONST_PLAYER_EVENT = cj.EVENT_PLAYER_LEAVE

---------------------
--   触发事件分发
---------------------

local action = function ()
    local p = player:h2o( GetTriggerPlayer() )

    local params = {
        player =  p,
    }

    player:update( eventName_active, params ) --任意玩家事件
    leave:update( p, params )

end

local trg = trigger:new( action )

---------------------
--    注册与排泄
---------------------

---@param p player
---@param Private_reReg bool 重新注册时跳过检测 强行注册
local function regEvent( p, Private_reReg )
    if Private_reReg then
        goto reReg
    end
    --检测是否已注册
    if p.events[mainEvent] then
        return
    end
    p.events[mainEvent] = true
    ::reReg::
    TriggerRegisterPlayerEvent( trg.handle, p.handle, CONST_PLAYER_EVENT )
end

--- 重新注册：排泄事件
local reReg = leave:new( '重新注册事件' )
reReg.on_update = function (event, params)
    trg:del()   --删除触发器重新创建：排泄所有事件
    trg = trigger:new(action)
    --重新为单位注册事件
    force:enum(function (u)
        if u.events[mainEvent] then
            regEvent( u, true )
        end
    end)
end

---------------------
--    任意事件
---------------------

local enable
enable = function ()
    evtInit = player:event_any( '玩家-初始化', onInit )
    evtInit.on_update = onInit
    enable = function () end
    --注册已存在的所有玩家
    force:enum(function (p)
        regEvent( p )
    end)
end
onInit = function (event, params)

    local p = params.player
    regEvent( p )

end

---------------------
--      接收器
---------------------

--收到注册：玩家-按下ESC
local reg_leave = leave:new( '注册事件' )
reg_leave.on_update = function ( event, params )
    local type = params.type
    if type == 'player' then
        ---@type player
        local p = params.player
        --注册事件
        regEvent( p )
        --返回一个事件
        params.ret = leave:new( p )
        --记录进对象中。销毁时方便销毁事件
        table_insert(p.events, params.ret)
    else--if type == 'any' then
        --注册任意事件时启用初始化事件捕捉
        enable()
    end

end
