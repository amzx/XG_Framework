---@class mDestructable
local mDestructable = {
    type = 'module',
    module = 'destructable',

}
---@class cDestructable 可破坏物类
---@field handle handle
---@field id string 字符串id
---@field _id int 魔兽整数id
---@field invulnerable bool 是否无敌
local cDestructable = {
    type = 'class',
    class = 'destructable',

}
cDestructable.__index = cDestructable
mDestructable.cDestructable = cDestructable
local DestructableRestoreLife = cj.DestructableRestoreLife
local SetDestructableLife = cj.SetDestructableLife
local GetDestructableLife = cj.GetDestructableLife
local KillDestructable = cj.KillDestructable
local SetDestructableAnimation = cj.SetDestructableAnimation
local SetDestructableInvulnerable = cj.SetDestructableInvulnerable
local IsDestructableInvulnerable = cj.IsDestructableInvulnerable

local destructable_n2i = function (name)
    return xslk.destructable:n2i(name)
end

local destructable_n2i_like = function (name)
    return xslk.destructable:n2i_like(name)[1]
end

local f_at = {
    ['@'] = destructable_n2i,
    ['*'] = destructable_n2i_like,
}

local iid_cache = { }
local sid_cache = { }
local function convert_id ( param )
    local at = f_at [ param:sub(1,1) ]
    if at then
        sid_cache[ param ] = at( param:sub(2) ) or 'I0t0'
    else
        sid_cache[ param ] =  param
    end
    local id = sid_cache[ param ]
    iid_cache [ id ] = base.iid( id )
    return id
end

local CreateDestructableZ = cj.CreateDestructableZ
local CreateDestructable = cj.CreateDestructable

--- 新建 可破坏物
function mDestructable:new( params )
        local id = params.id or 'I0t0'
        id = sid_cache[ id ] or convert_id( id )
        ---@class destructable
        local data = {
            type = 'destructable',
            id = id,

            ---@type bool 是否无敌
            invulnerable = false,

            ---@type event[]
            events = {},
            ---@type int id
            _id = iid_cache[ id ],
            ---@type real x轴
            x = params.x or 0,
            ---@type real y轴
            y = params.y or 0,
            ---@type real z轴 高度
            z = params.z or 0,
            ---@type real 面向角度
            face = params.face or 270.0,
            ---@type real 缩放 : 注意非框架创建的可破坏物无法获取正确数值 默认为1.0
            scale =  params.scale or 1.0,
            ---@type int 样式 : 注意非框架创建的可破坏物无法获取正确数值 默认为0
            variation = params.variation or 0
        }
        ---@type handle
        local h
        if params.z then
            h = CreateDestructableZ (
                data._id,
                data.x,
                data.y,
                data.z,
                data.face,
                data.scale,
                data.variation
            )
        else
            h = CreateDestructable (
                data._id,       --id
                data.x,        --x轴
                data.y,        --y轴
                data.face,     --面向角度
                data.scale,    --缩放
                data.variation --样式
            )
        end
        data.handle = h
        data.hid = cj.GetHandleId(h)

        setmetatable(data, cDestructable)

        data:init( )

        return data
end

---@private 初始化
function cDestructable:init(  )
    local slk = xslk.destructable( self.id )
    self.slk = slk
    self.maxHP = slk.HP
    mDestructable:update( '可破坏物-初始化',
        {
            destructable = self,
        }
    )
end

--- 复活 可破坏物
---@param birth bool 复活是否播放生长动画
---@param life real|nil 复活后的生命值 固定值 为nil或直接留空时 默认为最大生命值
function cDestructable:restore( birth, life)
    if not life then
        life = self.maxHP
    end
    local b = false
    if birth then
        b = true
    end
    DestructableRestoreLife( self.handle, life, b )
end

--- 设置 可破坏物 生命值
---@param life real 固定值
function cDestructable:setHP( life )
    SetDestructableLife( self.handle, life )
end

--- 获取 可破坏物 生命值
---@param maxHP bool 是否获取最大生命值
---@return real
function cDestructable:getHP( maxHP )
    return maxHP and self.maxHP or GetDestructableLife( self.handle )
end


local option_operration = setmetatable(
    {
        ['open'] = 'open',
        ['打开'] = 'open',
        ['close'] = 'close',
        ['关闭'] = 'close',
        ['destroy'] = 'destroy',
        ['破坏'] = 'destroy',
        ['摧毁'] = 'destroy',
    },
    { __index = function (_,_)
        return 'open'
    end
    }
)
--- 修改 可破坏物[大门] 状态
---@param operration string open|close|destroy 打开|关闭|破坏(摧毁)
---@return real
function cDestructable:gate( operration )
    local option = option_operration [ operration ]
    if option == 'open' then
        if self:getHP() > 0 then
            self:kill()
        end
        self:setAnim( "death alternate" )
    elseif option == 'close' then
        if self:getHP() <= 0 then
            self:restore(true)  --播放动画 复活大门
        end
        self:setAnim( "stand" )
    else--if option == 'destroy' then
        if self:getHP() > 0 then
            self:kill()
        end
        self:setAnim( "death" )
    end

end

--- 设置 可破坏物 动画
---@param anim string 动画名称
function cDestructable:setAnim( anim )
    SetDestructableAnimation( self.handle, anim )
end

--- 杀死 可破坏物
function cDestructable:kill( )
    KillDestructable( self.handle )
end

---设置 可破坏物 无敌/可攻击
---@param flag bool T:无敌  (默认)F:可攻击
function cDestructable:setInvulnerable( flag )
    local b = false
    if flag then
        b = true
    end
    SetDestructableInvulnerable( self.handle, b )
    self.invulnerable = b
end

---获取 可破坏物 是否无敌
---@return bool T:无敌 (默认)F:可攻击
function cDestructable:getInvulnerable(  )
    return self.invulnerable
end

return mDestructable