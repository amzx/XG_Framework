--[[
    可破坏物 事件
]]
---@class destructable
local cDestructable = destructable.cDestructable
local pool = eventpool:new('任意可破坏物事件') --新建事件池

---@class mDestructable
local destructable = destructable

---@private 通知事件更新
---@param eventName any 事件名
---@param params any 传递参数
function destructable:update( eventName, params )
    pool:update( eventName, params )
end


-- 对自己注册事件
---@param event_name string 事件名: 可破坏物- 详情查看module\unit\evt.lua
---@param func fun( event:event, params:table ) 回调函数 框架自带事件通常只有两个参数 1个event 一个table用于传参
---@return event|nil 如果事件不存在返回nil
function cDestructable:event( event_name, func )
    local params = {
        type = 'destructable',
        destructable = self,     --注册事件的对象
    }
    --注册单位类型事件
    eventpool:reg(
        event_name,    --通知指定的事件，有单位要注册事件
        params
    )
    --指定对象事件 通过params获取返回的事件
    local event = params.ret
    if event then
        event.on_update = func
    end
    return event
end


--注册[任意可破坏物]事件
---@param event_name string 事件名: 可破坏物-初始化 可破坏物-销毁数据  详情查看module\unit\evt.lua
---@param func fun( event:event, params ) 回调函数 框架自带事件通常只有两个参数 1个event 一个table用于传参
---@return event
function destructable:event_any( event_name, func )
    local params = {
        event = event_name,
        type = 'any',
    }
    --收到注册事件请求  注册事件
    eventpool:reg(
        event_name,
        params
    )

    --注册单位类型事件
    local event = pool:new( event_name )
    event.on_update = func
    return event
end

--注册[单位id]事件
---@param event_name string 事件名: 单位-初始化 单位-销毁数据 单位-伤害  单位-受伤 详情查看module\unit\evt.lua
---@param id string hpea
---@param func fun( event:event, params ) 回调函数 框架自带事件通常只有两个参数 1个event 一个table用于传参
---@return event|nil
function destructable:event_id( event_name, id, func )
    local params = {
        event = event_name,
        type = 'id',
        id = id
    }
    --收到注册事件请求  注册事件
    eventpool:reg(
        event_name,
        params
    )

    --注册单位类型事件
    local event = params.ret
    if event then
        event.on_update = func
    end
    return event
end

--- 单位事件 垃圾收集
---@param event_name string 框架衍生的事件(单位-伤害 物品-被丢弃)不支持gc，只支持原生事件 例如 单位-受伤 单位-丢弃物品
function destructable:event_gc( event_name )
    eventpool:reReg( event_name )
end
