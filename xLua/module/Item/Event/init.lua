--[[

'物品-被使用',
'物品-被获得',
'物品-被丢弃',
'物品-被抵押',
'物品-被出售'

]]
---@class item
local cItem = item.cItem
local pool = eventpool:new('任意物品事件') --新建事件池

---@private 通知事件更新
---@param eventName any 事件名
---@param params any 传递参数
function item:update( eventName, params )
    pool:update( eventName, params )
end


-- 对[物品]自己注册事件
---@param event_name string 事件名:  物品-被丢弃 物品-被获得 物品-被抵押 详情查看module\item\event
---@param func fun( event:event, params:table ) 回调函数 框架自带事件通常只有两个参数 1个event 一个table用于传参
---@return event|nil 如果事件不存在返回nil
function cItem:event( event_name, func )
    local params = {
        type = 'item',
        item = self,     --注册事件的物品
    }
    --注册单位类型事件
    eventpool:reg(
        event_name,    --通知指定的事件，有单位要注册事件
        params
    )
    --指定对象事件 通过params获取返回的事件
    local event = params.ret
    if event then
        event.on_update = func
    end
    return event
end


--注册[任意物品]事件
---@param event_name string 事件名: 物品-初始化 物品-销毁数据 物品-被丢弃  物品-被获得 物品-被抵押 详情查看module\item\event
---@param func fun( event:event, params:table ) 回调函数 框架自带事件通常只有两个参数 1个event 一个table用于传参
---@return event
function item:event_any( event_name, func )
    local params = {
        event = event_name,
        type = 'any',
    }
    --收到注册事件请求  注册事件
    eventpool:reg(
        event_name,
        params
    )

    local event = pool:new( event_name )
    event.on_update = func
    return event
end
--注册[物品id]事件
---@param event_name string 事件名: 物品-初始化 物品-销毁数据 物品-被丢弃  物品-被获得 物品-被抵押 详情查看module\item\event
---@param id string afac
---@param func fun( event:event, params:table ) 回调函数 框架自带事件通常只有两个参数 1个event 一个table用于传参
---@return event|nil
function item:event_id( event_name, id, func )
    local params = {
        event = event_name,
        type = 'id',
        id = id
    }
    --收到注册事件请求  注册事件
    eventpool:reg(
        event_name,
        params
    )

    local event = params.ret
    if event then
        event.on_update = func
    end
    return event
end

-- require 'xLua.module.unit.event.death'
-- require 'xLua.module.unit.event.damaged'

-- require 'xLua.module.unit.event.useItem'
-- require 'xLua.module.unit.event.pickItem'
-- require 'xLua.module.unit.event.dropItem'
-- require 'xLua.module.unit.event.pawnItem'
