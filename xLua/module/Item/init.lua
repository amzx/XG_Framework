--[[
    物品
]]
local CreateItem = cj.CreateItem
local SetItemVisible = cj.SetItemVisible
local SetItemPosition = cj.SetItemPosition
local SetWidgetLife = cj.SetWidgetLife
local GetWidgetLife = cj.GetWidgetLife
local SetItemCharges = cj.SetItemCharges
local GetItemCharges = cj.GetItemCharges
local SetItemInvulnerable = cj.SetItemInvulnerable
local SetItemPawnable = cj.SetItemPawnable
local SetItemDroppable = cj.SetItemDroppable
local RemoveItem = cj.RemoveItem


---@class cItem
---@field public handle handle 物品句柄
---@field public id string 字符串id
---@field public name string 物品名称
local item = {
    type = 'class',
    class = 'item',
    _h2o = {},

    -------------------默认值---------------------
    Visible = true, --默认可见
    Invulnerable = false, --默认不无敌

    --实际得看slk设置
    pawnable = true, --默认可抵押
    droppable = true, --默认可丢弃
}
item.__index = item

---@class m_Item
---@field public update fun(self,eventName,params) 更新通知
local module = {

    cItem = item
}
module.__index = module



----------------------------------------
--              动态缓存
----------------------------------------

local item_n2i = function (name)
    return xslk.item:n2i(name)
end

local item_n2i_like = function (name)
    return xslk.item:n2i_like(name)
end

local f_at = {
    ['@'] = item_n2i,
    ['*'] = item_n2i_like,
}

local iid_cache = { }
local sid_cache = { }
local function convert_id ( param )
    local at = f_at [ param:sub(1,1) ]
    if at then
        sid_cache[ param ] = at( param:sub(2) ) or 'afac'
    else
        sid_cache[ param ] =  param
    end
    local id = sid_cache[ param ]
    iid_cache [ id ] = base.iid( id )
    return id
end
----------------------------------------

---@ 新建物品 id 支持 at准确名称[@攻击之爪] like模式[*攻击] 以及 标准字符串id[ratc]
---@param params table => id, x, y
---@return item
function module:new( params )
    ---@type item
    local obj = self:generate( params )
    
    local h = CreateItem( obj._id, params.x or 0,params.y or 0 )
    
    obj:bind_handle( h )

    obj:init()

    return obj
end

---@private 绑定handle
function item:bind_handle( h )

    self.handle = h
    self.hid = cj.GetHandleId(h)

    item._h2o[ self.hid ] = self

end

---@private
---@return item
function module:generate( params )
    local id = params.id or 'afac'
    id = sid_cache[ id ] or convert_id( id )
    ---@class item : cItem
    local obj = {
        type = 'item',
        id = id, --物品id
        _id = iid_cache[ id ], --数字id
        owner = nil, --所属玩家
    }
    setmetatable( obj, item)
    return obj
end

---@private 初始化物品数据
function item:init()
    local slk = xslk.item( self.id )
    self.slk = slk
    ---@type bool
    self.powerup = slk.powerup  == "1"
    self.pawnable = slk.pawnable == "1"
    self.droppable = slk.droppable == "1"
    self.name = slk.Name
    ---@type event[]
    self.events = {}
    module:update( '物品-初始化',
        {
            item = self,
        }
    )
end

--将handle转换为item对象
---@return item
function module:h2o( h )
    local hid = cj.GetHandleId( h )
    if hid == 0 then return end
    if item._h2o[ hid ] then return item._h2o[ hid ] end
    local iid = cj.GetItemTypeId( h )
    local obj = module:generate( { id = base.sid( iid ) } )
    obj:bind_handle( h )
    obj:init()
    return obj
end

---@ 物品是否可见
---@return bool
function item:isVisible( )
    return self.Visible
end

---@ 设置物品可见
---@param v bool|any 是否可见
---@return bool
function item:setVisible( v )
    local value
    if v then
        value = true
    else
        value = false
    end
    self.Visible = value
    SetItemVisible( self.handle, false )
    return value
end

---@ 设置物品坐标
---@param x real    [可选]x坐标
---@param y real    [可选]y坐标
function item:setXY( x, y )
    if x then
        self.x = x
    end
    if y then
        self.y = y
    end
    SetItemPosition( self.handle, self.x, self.y )
end

---@ 获取物品X坐标
function item:getX()
    return self.x
end

---@ 获取物品Y坐标
function item:getY()
    return self.y
end

---@ 设置物品生命值
---@param hp real    生命值
function item:setHP( hp )
    SetWidgetLife( self.handle, hp )
end

---@ 获取物品生命值
---@return  real
function item:getHP( )
    return GetWidgetLife( self.handle )
end

---@ 设置物品使用次数
---@param num int    使用次数
function item:setCharges( num )
    SetItemCharges( self.handle, num )
end

---@ 获取物品使用次数
---@return int
function item:getCharges( )
    return GetItemCharges( self.handle )
end

---@ 设置物品 无敌
---@param flag bool    是否无敌
function item:setInvul( flag )
    local b
    if flag then
        b = true
    else
        b = false
    end
    self.Invulnerable = b
    SetItemInvulnerable( self.handle, b )
end

---@ 获取物品是否 无敌
---@return bool
function item:getInvul( )
    return self.Invulnerable
end

---@ 设置物品 是否可抵押
---@param flag bool  T:可抵押  F:不可抵押
function item:setPawnable( flag )
    local b
    if flag then
        b = true
    else
        b = false
    end
    self.pawnable = b
    SetItemPawnable( self.handle, b )
end

---@ 获取物品是否 可抵押
---@return bool
function item:getPawnable( )
    return self.pawnable
end

---@ 设置物品 是否可丢弃 [只对玩家操作影响，不影响使用代码丢弃]
---@param flag bool  T:可丢弃  F:不可丢弃
function item:setDroppable( flag )
    local b
    if flag then
        b = true
    else
        b = false
    end
    self.droppable = b
    SetItemDroppable( self.handle, b )
end

---@ 获取物品是否 可丢弃 [只对玩家操作影响，不影响使用代码丢弃]
---@return bool
function item:getDroppable( )
    return self.droppable
end

--- 删除物品并排泄
function item:del()
    module:update( '物品-销毁数据',
        {
            item = self,
        }
    )

    for i, v in ipairs(self.events) do
        if v then
            v:del() --销毁事件
        end
    end
    item._h2o[ self.hid ] = nil
    setmetatable(self, nil)
    SetWidgetLife(self.handle, 0.401)
    RemoveItem( self.handle )
    self.slk = nil
    self.handle = nil
    self.hid = 0
    self.events = nil
end

return module