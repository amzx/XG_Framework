--[[
    UI模块
]]
require "xLua.module.ui.base"
require "xLua.module.ui.img"
require "xLua.module.ui.text"
require "xLua.module.ui.edit"

require "xLua.module.ui.icon"
require "xLua.module.ui.slot"
require "xLua.module.ui.button"
require "xLua.module.ui.radiobox"
require "xLua.module.ui.checkbox"
require "xLua.module.ui.verticalscrollbar"


require 'xLua.module.ui.event'



require "xLua.module.ui.panel"

require 'xLua.module.ui.Window'
require 'xLua.module.ui.tooltip'


require 'xLua.module.ui.game'
--[=[
require 'xLua.module.timer'

local DzFrameEditBlackBorders    = japi.DzFrameEditBlackBorders
local DzLoadToc                  = japi.DzLoadToc
local DzFrameShow                = japi.DzFrameShow
local DzFrameGetMinimapButton    = japi.DzFrameGetMinimapButton
local DzFrameGetMinimap          = japi.DzFrameGetMinimap
local DzFrameClearAllPoints      = japi.DzFrameClearAllPoints
local DzFrameSetPoint            = japi.DzFrameSetPoint
local DzFrameSetSize             = japi.DzFrameSetSize
local DzFrameGetTooltip          = japi.DzFrameGetTooltip
local DzFrameGetChatMessage      = japi.DzFrameGetChatMessage
local DzFrameGetUnitMessage      = japi.DzFrameGetUnitMessage
local DzFrameGetCommandBarButton = japi.DzFrameGetCommandBarButton
local DzFrameGetItemBarButton    = japi.DzFrameGetItemBarButton

--DzFrameHideInterface()
--DzFrameEditBlackBorders( 0, 0 )
DzLoadToc( "UI\\xglist.toc" )
]=]
