--[[
    雪月框架[Mini]    -   入口
    该Mini入口仅启动以下功能
        xSlk
        xRect
        Timer
        Sync
        以及基础函数
]]
            require 'xLua.Version'

            --require 'xLua.Message'          --jass message的HOOK 主要鼠标键盘类

            require 'xLua.Common'

            require "xLua.Env" --全局环境 --根据编译的版本 编译器会自动复制对应的环境

            require 'xLua.JassFix'           --修复cj库
            ---@comment level 为 0 handle 会变成 int
            if require 'jass.runtime'.handle_level == 0 then
                cj.GetHandleId = function (h)
                    return h or 0
                end
            end
            require 'xLua.cache.Init'           --缓存

timer     = require "xLua.module.Timer"         --中心计时器

            require "xLua.module.Sync"          --同步数据

rect      = require 'xLua.module.Rect'          --Rect

            require 'xRect'                 --预处理xRect

            require 'Mods'

--- 定期启动Lua的垃圾收集器 默认60秒
--- 通过设置gc.maxcycle = sec * 100 来改变回收速率
gc = timer:loop( 60, function (self)
    print('****定时释放内存****')
    local cur = collectgarbage("count")
    collectgarbage("collect")
    cur = cur - collectgarbage("count")
    printf( 'gc 释放:\t%f kb', cur )
end)


print('\t————\t雪月框架[Mini] - 加载完成\t————')