local GetRandomInt = cj.GetRandomInt
local GetRandomReal = cj.GetRandomReal

math.random = function (m,n)
    if not m and not n then
        return GetRandomReal(0,1)
    end
    if not m or not n then
        return GetRandomInt(1, m or n)
    end
    return GetRandomInt(m,n)
end

--- 随机整数
---@param m int m为nil时 0 ~ 1
---@param n int n为nil时 1 ~ m
---@return int
math.randomInt = function ( m, n )
    if not n then
        n = m
        m = 1
    elseif not m then
        m = 0
    end
    return GetRandomInt( m, n )
end

--- 随机实数
---@param m real m为nil时 0 ~ 1
---@param n real n为nil时 0.01 ~ m
---@return real
math.randomReal = function ( m, n )
    if not n then
        n = m
        m = 0.01
    elseif not m then
        m = 0
    end

    return GetRandomReal( m, n )
end

math.sin   = cj.Sin
math.cos   = cj.Cos
math.tan   = cj.Tan

math.asin  = cj.Asin
math.acos  = cj.Acos
math.atan  = cj.Atan
math.atan2 = cj.Atan2

math.pow   = cj.Pow
math.sqrt  = cj.SquareRoot


local meta_k = { __mode = 'k', }
local meta_v = { __mode = 'v', }
local meta_kv = { __mode = 'kv', }

---创建或更改表键为弱引用从而实现自动排泄
---意思是当t作为key为弱引用，当你把t作为其他表的key时，gc会无视他的引用
---@param t table
---@return table
table.k = function( t )
    local mt = getmetatable(t)
    if mt then
        mt.__mode = 'k'
        return t
    else
        mt = {
            __mode = 'k'
        }
    end
    return setmetatable( t or {}, meta_k )
end

---创建或更改表的键值为弱引用从而实现自动排泄
---意思是当t作为value为弱引用，当你把t作为其他表的value时，gc会无视他的引用
---@param t table
---@return table
table.v = function ( t )
    local mt = getmetatable(t)
    if mt then
        mt.__mode = 'v'
        return t
    else
        mt = {
            __mode = 'v'
        }
    end
    return setmetatable( t or {}, meta_v )
end

---创建或更改表键与值为弱引用从而实现自动排泄
---意思是当t作为key或者value时都为弱引用。也就是gc不管你是否在引用
---@param t table
---@return table
table.kv = function( t )
    local mt = getmetatable(t)
    if mt then
        mt.__mode = 'kv'
        return t
    else
        mt = {
            __mode = 'kv'
        }
    end
    return setmetatable( t or {}, meta_kv )
end

local table_concat = table.concat
base = {
    ---转化为字符串id
    ---@param s int
    sid = function ( s )
        s = s // 1 | 0
        local result = {}
        local i = 0
        local x = 1
        while x ~= 0 do
            x =  s % 256 --余数
            s = ( s - x ) / 256 --下一个除数
            i = i + 1
            result [ i ] = (x..''):char()
        end
        result [ i ] = nil --去掉末尾0
        return table_concat(result):reverse()
    end,
    ---转化为整数id
    ---@param s string
    iid = function( s )
        assert(type(s)=='string','base.iid 传入了一个错误的参数类型')
        local id = 0
        for i=1 , #s do
            id = id * 256 + s:byte(i)
        end
        return id
    end,
    B2I = function (b)
        if b then
            return 1
        end
        return 0
    end,
    I2B = function (i)
        if i==0 then
            return false
        end
        return true
    end,
    ---将函数返回值保存为table
    ---例如 table = Params2Table( str:match( "(%d-),(%d)" ) ) 多个返回值存为表
    Params2Table = function ( ... )
        return {...}
    end,

}