--[[
    雪月框架    -   入口
]]
xLua = {
    --当前环境 有以下三种
    --debug|test|release
    --默认为release 由于框架会动态编译 需要在 require 'xLua.Env' 加载全局环境后 才可以获取到正确的值
    Env = 'release',
    Version = '1.0',
}

---@alias func function function
---@alias fun function function
---@alias int int integer 整数
---@alias bool bool 布尔值

cj      =   require "jass.common"
hook    =   require "jass.hook"
japi    =   require 'jass.japi'
G       =   require 'jass.globals'
jslk    =   require 'jass.slk'
