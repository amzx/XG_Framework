local mtMsg = require 'jass.message'
local data = {}
message = setmetatable({}, {
    __index = function (_,k)
        return mtMsg[k] or data[k]
    end,
    __newindex = function (_,key,value)
        if mtMsg[key] or key == 'hook' then
            mtMsg[key] = value
            return
        end
        data[key] = value
    end
} )

local defaultHook = function (msg)
    return true
end

local msgHook = setmetatable(
{
--[[
    ['mouse_down'] = function (msg) --鼠标在 按下 单位或地面时
        local code = msg.code   -- 左键为 1  右键为 4
        local x, y = msg.x, msg.y --鼠标世界坐标

        return true
    end,
    ['mouse_up'] = function (msg) --鼠标弹起

        return true
    end,

    ['mouse_ability'] = function (msg) --当鼠标点击技能按钮事件
        local ability_id = msg.ability
        local order_id = msg.order

        return true
    end,

    ['key_down'] = function (msg) --键盘按下事件  不会跟聊天框冲突
        local code = msg.code --整数 键码
        local state = msg.state -- 默认为0  判断组合键用 alt + 字母键 时  为 4  当 ctrl + 字母键时 为 2  shift 1

        --修改 msg.code 可以进行改键
        msg.code = code

        return true
    end,
    ['key_up'] = function (msg) --键盘弹起事件  不会跟聊天框冲突 
        local code = msg.code --整数 键码
        local state = msg.state -- 默认为0  判断组合键用 alt + 字母键 时  为 4  当 ctrl + 字母键时 为 2

        --修改 msg.code 可以进行改键
        msg.code = code

        return true
    end,
]]
},{__index = function (_,_)
    return defaultHook
end})

message.msgHook = msgHook

message.hook = function (msg)
    return msgHook[msg.type] ( msg )
end

--[[
设置 message.msgHook['mouse_down'] = function(msg)

    ......
    ......

    return true
end
即可HOOK鼠标按下事件，对其返回FALSE的话，魔兽将不会处理他

]]