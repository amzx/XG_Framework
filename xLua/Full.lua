
            require 'xLua.JassFix'           --修复cj库

            ---@comment level 为 0 handle 会变成 int
            if require 'jass.runtime'.handle_level == 0 then
                cj.GetHandleId = function (h)
                    return h or 0
                end
            end

            -- msg.hook 和 kkapi 冲突 导致点技能栏图标会崩溃
            -- 依赖于该库的热键库已采用新方案，使用方法不变
            --require 'xLua.Message'          --jass message的HOOK 主要鼠标键盘类

            require 'xLua.cache.Init'           --缓存

queue     = require 'xLua.module.Queue'         --队列模块

timer     = require "xLua.module.Timer"         --中心计时器

            require "xLua.module.Sync"          --同步数据

trigger   = require "xLua.module.Trigger"       --触发器

eventpool = require "xLua.module.EventPool"     --事件池

rect      = require 'xLua.module.Rect'          --Rect

            require 'xLua.module.Player'        --玩家

item    =   require 'xLua.module.Item'          --物品

            require 'xLua.module.Unit'          --单位

destructable = require 'xLua.module.Destructable'   --可破坏物

fog     =   require 'xLua.module.Fog'           --可见度修正器

window  =   require 'xLua.module.Window'        --窗口

            require 'xLua.module.Event'         --事件

            require 'xLua.module.extends'       --扩展内容

            require 'xRect'                 --预处理xRect

keyboard =  require 'xLua.module.Keyboard'      --键盘事件

            require 'Mods'

camera = require 'xLua.module.Camera'

japi.DzEnableWideScreen(true)
camera.SetWideScreen(true)
---设置黑边范围;
camera.BlackBorders(0.020,0.130)

print '------cam loaded--------'

--- 定期启动Lua的垃圾收集器 默认60秒
--- 通过设置gc.cycle = sec * 100 来改变回收速率
gc = timer:loop( 60, function (self)
    print('****定时释放内存****')
    local cur = collectgarbage("count")
    collectgarbage("collect")
    cur = cur - collectgarbage("count")
    printf( 'gc 释放:\t%f kb', cur )
end)


print('\t————\t雪月框架 - 加载完成\t————')