---@diagnostic disable: missing-fields
local __cj = require 'jass.common'

--- **********  该模块修复以下问题 ************
--- CJ库无引用导致闭包函数被GC
--- 每次获取CJ库内的函数都会重新加载
--- *****************************************

---@type common
local t = setmetatable({}, {__index = function (t, k)
    local o = __cj[k]
    if o then
        t[k] = o
    end
    return o
end})

for key, value in pairs(__cj) do
    t[key] = value
end

package.loaded['jass.common'] = t

local TimerStart = t.TimerStart
t.TimerStart = function (timer, timeout, periodic, handlerFunc)
    TimerStart(timer, timeout, periodic, handlerFunc)
end

local ForForce = t.ForForce
t.ForForce = function (force, func)
    ForForce(force, func)
end

local ForGroup = t.ForGroup
t.ForGroup = function (group, func)
    ForGroup(group, func)
end

local TriggerAddAction = t.TriggerAddAction
t.TriggerAddAction = function (trigger, action)
    TriggerAddAction(trigger, action)
end

local TriggerAddCondition = t.TriggerAddCondition
t.TriggerAddCondition = function (trigger, condition)
    TriggerAddCondition(trigger, condition)
end

local Filter = t.Filter
t.Filter = function ( code )
    return Filter(code)
end

local Condition = t.Condition
t.Condition = function ( code )
    return Condition(code)
end

local EnumDestructablesInRect = t.EnumDestructablesInRect
t.EnumDestructablesInRect = function ( rect, filter, action )
    EnumDestructablesInRect(rect, filter, action)
end

local EnumItemsInRect = t.EnumItemsInRect
t.EnumItemsInRect = function ( rect, filter, action )
    EnumItemsInRect(rect, filter, action)
end

if cj then -- 更换全局变量的引用表
    cj = t
end


local __japi = require 'jass.japi'
t = setmetatable({}, {__index = function (t, k)
    local o = __japi[k]
    if o then
        t[k] = o
    end
    return o
end})
for key, value in pairs(__japi) do
    t[key] = value
end
package.loaded['jass.japi'] = t

if japi then -- 更换全局变量的引用表
    japi = t
end

local __ai = require 'jass.ai'
t = setmetatable({}, {__index = function (t, k)
    local o = __ai[k]
    if o then
        t[k] = o
    end
    return o
end})
for key, value in pairs(__ai) do
    t[key] = value
end
package.loaded['jass.ai'] = t



return {

}
