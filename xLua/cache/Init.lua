--[[
    雪月框架 - 缓存初始化
]]

require 'xLua.cache.FuncDec'  --常用函数的定义

require 'xLua.cache.Const' --常量

require 'xLua.cache.xslk'  --载入xslk库

require 'xLua.cache.xRect'  --xRect 预处理编辑器内的区域

