local mt = {}
mt.__index = mt
xslk = {}
local slk_type = { 
    ability = {prefix = 'a'},
    buff = {prefix = 'b'},
    destructable = {prefix = 'd'},
    doodad = {prefix = 'c'},
    item = {prefix = 'i'},
    misc = {prefix = 'm'},
    txt = {prefix = 't'},
    unit = {prefix = 'u'},
    upgrade = {prefix = 'g'},
}
local slk_list = { 'ability', 'buff', 'destructable', 'doodad', 'item', 'misc','txt', 'unit',  'upgrade' }

--初始化物编类别
for _, v in ipairs(slk_list) do
    local t = {
        type = v,
        count = 0, --用来作为动态物编数量
        _id = 0,    --用来计算id
        prefix = slk_type[v].prefix, --前缀
        _i2o = {}, --使用id指向一个物编表 [id] = xslk['unit']['hpea']

        --_ids = {}, --由xslk自动生成 数组 id列表
        --_n2i = {}, --同上 散列 [name] = id  如果存在同名物编则是后面这种情况 [name] = {id,id……}
        --_dynmic = {}, --和ids不同的是 这里只存动态物编id 供new动态缓存使用   数组 id列表
    }

    setmetatable(t, mt)
    xslk[v] = t
    setmetatable(t._i2o, {__index = function (_,id)
        return jslk[t.type][id]
    end})
end

---@private 缓存静态物编
function mt:static( id, parent )
    --if id == parent then
    --    return
    --end
    local slk = jslk[self.type]

    --先读自己的数据，没有再访问父模板
    local l = setmetatable({}, { __index = function (_, k)
        local o = jslk[self.type][id]
        return o and o[k] or slk[parent][k]
    end })

    l.id = id
    l._parent = parent
    self._i2o[ id ] = l
end

require 'xslk.Cache' --载入xslk保存自动生成的缓存

--新建物编对象缓存
function mt:new(slk)
    local _type = self.type --'ability', 'buff', 'destructable', 'doodad', 'item', 'misc', 'txt', 'unit', 'upgrade'
    local id = slk.id
    
    if not id then --无指定ID：查找id
        self.count = self.count + 1
        id = self._dynmic[self.count] --这就显得_dynmic按顺序生成很重要
    end
    slk.id = id

    self._i2o[ id ] = slk
    --自定义物编模板
    local template = self._i2o[ slk._parent ]
    if template._parent then
        --追溯上一层模板，再由上一层模板溯源
        setmetatable(slk, { __index = function (_, k)
            return template[k]
        end })
    else
        setmetatable(slk, { __index = function (t, k)
            return jslk[_type][t._parent][k]
        end })
    end

    return id
end
local rules = {}
xslk.item.rules = rules
function xslk.item:rule(  params )
    table_insert( rules, params )
    if params.flag == '混合' then
        --混合开关 用于判断 注入代码
        self.Mix = true
    end
end

-- xslk\slk\obj\attr

--exmp: xslk.unit:n2i('农民')
--用名字获取物编ID，要求是一个准确的名字
function mt:n2i(name)
    --self: slk
    local id = self._n2i[name]
    if not id then
        return   --不存在该名字 返回空字符串 或 nil(可以触发报错)
    end

    return id  --如果有重名 那么id是一个table 如果没有重名 那么就是字符串
end

--exmp: xslk.unit:n2i_like('农') --会返回名字中带有农的
--用名字获取物编ID
---@param name string Name关键词
---@param ret_tb boolean  true:将找到的所有单位列个表格返回  false:返回找到的第一个
function mt:n2i_like(name,ret_tb)
    --self: slk
    local ids = {}
    local i = 0
    if ret_tb then

        for _, id in ipairs(self._ids) do
            local slk = self._i2o[id]
            if (slk.Name or ''):match(name) then
                i = i + 1
                ids [i] = id
            end
        end

    else

        for _, id in ipairs(self._ids) do
            local slk = self._i2o[id]
            if (slk.Name or ''):match(name) and not ret_tb then
                return id
            end
        end

    end

    return ids
end

require 'xslk.init'  --加载动态物编创建过程

mt.new = nil --删除new方法

function mt:get(id)
    return self._i2o[id]
end

mt.__call = mt.get