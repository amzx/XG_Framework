xrect = {
    _names = { },
    _n2o = { },
}

local cache_n2o = {}

local function at( name )
    for index, match in ipairs(xrect._names) do
        if match == name  then
            return xrect._n2o[index]
        end
    end
end

local function match( name )
    for index, match in ipairs(xrect._names) do
        if match:match( name ) then
            return xrect._n2o[index]
        end
    end
end

local map_at = {
    ['*'] = match,
}

local function xrect_n2o( name )
    local prefix = name:sub(1,1)
    local func = map_at[prefix]
    local ret
    if func then
        ret = func( name:sub(2) )
    else
        ret = at( name )
    end

    cache_n2o[name] = ret
    return ret
end

-- 名字转化为rect对象 以*开头支持模式匹配 或直接填写全名
function xrect:n2o( name )
    return cache_n2o[name] or xrect_n2o( name )
end