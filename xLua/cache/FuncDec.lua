--[[
    雪月框架 - 常用函数初始化
    这里一般存放无需其他特殊方法支持的函数
    
]]

local ipairs = ipairs
local type = type
local tostring = tostring

--分割字符串
--@param str string 完整的字符串
--@param split_char string 分隔符
--@return table 返回一个表
function string.split(str, split_char)--分割字符串
    if not(str and split_char) then return {} end
    local sub_str_tab = {}
    while true do
        local pos,pos2 = str:find( split_char, 1, true) --四号参数 设为true 则作为普通文本处理 而不是模式匹配
        if not pos then
            if str ~= '' then
                table.insert(sub_str_tab, str)
            end
            break
        end
        local sub_str = str:sub(1, pos - 1)
        table.insert( sub_str_tab, sub_str )
        str = str:sub( pos2 + 1 )
    end
    return sub_str_tab
end


--附加数组[在原表基础上][只支持数组类型表(即有序表)]
--将list表的项目附加t
--@param t table 原表
--@param list table 被复制的表
--@return number 返回t因附加所增加的大小
function table.attach(t, list)
    if type(t)~='table' or type(list)~='table'then return end --检验参数类型
    local max_t = table.len(t)
    local rec = max_t
    for _,v_list in ipairs(list) do
        max_t = max_t + 1
        t[ max_t ] = v_list
    end
    t[0] = max_t
    return max_t - rec
end

--附加数组[生成新表][只支持数组类型表(即有序表)]
--将list表的项目附加t
--@param t table 原表
--@param list table 被复制的表
--@return table 返回最终合成的表
function table.attach_new(t, list)
    
    if type(t)~='table' or type(list)~='table'then return t end --检验参数类型
    local nt = {}
    local i = 0
    for _,v_list in ipairs(t) do
        i = i + 1
        nt[ i ] = v_list
    end
    for _,v_list in ipairs(list) do
        i = i + 1
        nt[ i ] = v_list
    end
    nt[0] = i
    return nt
end

--取表长度[兼容array和普通table]
--如果table中有[0]索引 默认返回[0](用作储存数组大小)
--和#t不同的是table.len遇到断开的索引就会停止查询
--@param t table 要取长度的表
function table.len(t)
    if type(t)~='table' then return 0 end --检验参数类型
    local rtn = t[0] or 0
    if rtn == 0 then
        for _,_ in ipairs(t) do
            rtn = rtn + 1
        end
    end
    return rtn
end

---加入一个元素到尾部(尾部id由tb[0]决定)
---你可以使用tb[0]直接获取长度
---@param tb table
---@param val any
function table_insert(tb,val)
    local c = (tb[0] or 0) + 1
    tb[0] = c
    tb[c] = val
end

local cache_bytes = {}
function bytes(str)
    if cache_bytes[str] then
        return cache_bytes[str]
    end

    local bt = { str:byte( 1, #str ) }
    local c = #bt
    local result = 0
    for i=1,c do
        result = result*10 + bt[i]
    end

    cache_bytes[str] = result
    return result
end

---选择排序 一般用来给pairs返回的列表进行排序
---舍弃 除了string与number类型的值
---@param t table<int,string|number>
function select_sort(t)
    local count = #t
    local i = 1
    local j = 1

    local a,b

    while i <= count-1 do
        local tp1 = type(t[i])

        if tp1 ~= 'string' and tp1 ~= 'number' then
            t[i] = t[count]
            t[count] = nil
            count = count - 1
            i=i-1
        else

            a = bytes( tostring( t[i] ) )

            j = i + 1
            while j <= count do
                local tp2 = type(t[j])
                if tp2 ~= 'string' and tp2 ~= 'number' then
                    t[j] = t[count]
                    t[count] = nil
                    count = count - 1
                    j=j-1
                else
                    b = bytes( tostring( t[j] ) )

                    if a > b then
                        a = b
                        t[i], t[j] = t[j],t[i]
                    end


                end

                j=j+1
            end

        end

       i = i + 1
    end

end

---排序遍历迭代器
---与pairs不同的是。他只会列出string和number类型的key
---并排序，使不同设备访问k,v时的顺序能够相同
---
---经过随机字符碰撞测试,字符串长度小于7时容易哈希碰撞(越短越容易)
---
---但平常使用的key不同于随机字符串，也难以出现哈希碰撞,仅需避免 kv 和 iv 混用,否则短字符可能会和index碰撞
---@return func iterator迭代器
function xPairs( tb )
    assert(type(tb) == 'table', debug.traceback("table expected"))
    local keys = {}
    local count = 0
    for k, v in pairs(tb) do
        count = count + 1
        keys[count] = k
    end
    select_sort(keys)
    local i = 0
    return function ()
        i = i + 1
        local k = keys[i]
        if i <= count then
            return k, tb[ k ]
        end
    end
end

---实例在table中对应的索引号，从1开始失败返回0
---@param tb table
---@param instance any 具体实例
---@return int instance在tb中的索引 失败返回0
function table.getIndex( tb, instance )
    for i, v in ipairs(tb) do
        if v == instance then
            return i
        end
    end
    return 0
end
