globals
//globals from cjLib75hJKJ374s4e597nba9o7w45gf:
constant boolean LIBRARY_cjLib75hJKJ374s4e597nba9o7w45gf=true
group cj_tmpgr_copy_nw509ert7
//endglobals from cjLib75hJKJ374s4e597nba9o7w45gf
//globals from cjLibw560nbs9b8nse46703948:
constant boolean LIBRARY_cjLibw560nbs9b8nse46703948=true
boolexpr cj_true_bool_4896bnao87
//endglobals from cjLibw560nbs9b8nse46703948
rect gg_rct_____________u=null
trigger gg_trg_______u=null

trigger l__library_init

//JASSHelper struct globals:

endglobals

    
//library cjLib75hJKJ374s4e597nba9o7w45gf:
function cj_group_copy_75hJKJ3745gf takes nothing returns nothing
//# optional
call GroupAddUnit(cj_tmpgr_copy_nw509ert7, GetEnumUnit())
endfunction

//library cjLib75hJKJ374s4e597nba9o7w45gf ends
//library cjLibw560nbs9b8nse46703948:
function cj_true_a497bnsor7 takes nothing returns boolean
//# optional
return true
endfunction
function cjLibw560nbs9b8nse46703948__init takes nothing returns nothing
set cj_true_bool_4896bnao87=Condition(function cj_true_a497bnsor7)
endfunction

//library cjLibw560nbs9b8nse46703948 ends
function InitGlobals takes nothing returns nothing
endfunction
function CreateUnitsForPlayer1 takes nothing returns nothing
local player p=Player(1)
local unit u
local integer unitID
local trigger t
local real life
set u=CreateUnit(p, 0x68666F6F, 1039.3, - 344.9, 231.236)
endfunction
function CreatePlayerBuildings takes nothing returns nothing
endfunction
function CreatePlayerUnits takes nothing returns nothing
call CreateUnitsForPlayer1()
endfunction
function CreateAllUnits takes nothing returns nothing
call CreatePlayerBuildings()
call CreatePlayerUnits()
endfunction
function CreateRegions takes nothing returns nothing
local weathereffect we
set gg_rct_____________u=Rect(896.0, - 640.0, 1248.0, - 96.0)
endfunction
function Trig_______uActions takes nothing returns nothing
endfunction
function InitTrig_______u takes nothing returns nothing
set gg_trg_______u=CreateTrigger()
call TriggerAddAction(gg_trg_______u, function Trig_______uActions)
endfunction
function InitCustomTriggers takes nothing returns nothing
call InitTrig_______u()
endfunction
function InitCustomPlayerSlots takes nothing returns nothing
call SetPlayerStartLocation(Player(0), 0)
call SetPlayerColor(Player(0), ConvertPlayerColor(0))
call SetPlayerRacePreference(Player(0), RACE_PREF_HUMAN)
call SetPlayerRaceSelectable(Player(0), true)
call SetPlayerController(Player(0), MAP_CONTROL_USER)
call SetPlayerStartLocation(Player(1), 1)
call SetPlayerColor(Player(1), ConvertPlayerColor(1))
call SetPlayerRacePreference(Player(1), RACE_PREF_ORC)
call SetPlayerRaceSelectable(Player(1), true)
call SetPlayerController(Player(1), MAP_CONTROL_COMPUTER)
endfunction
function InitCustomTeams takes nothing returns nothing
call SetPlayerTeam(Player(0), 0)
call SetPlayerTeam(Player(1), 0)
endfunction
function main takes nothing returns nothing

call SetCameraBounds(- 1280.0 + GetCameraMargin(CAMERA_MARGIN_LEFT), - 1536.0 + GetCameraMargin(CAMERA_MARGIN_BOTTOM), 1280.0 - GetCameraMargin(CAMERA_MARGIN_RIGHT), 1024.0 - GetCameraMargin(CAMERA_MARGIN_TOP), - 1280.0 + GetCameraMargin(CAMERA_MARGIN_LEFT), 1024.0 - GetCameraMargin(CAMERA_MARGIN_TOP), 1280.0 - GetCameraMargin(CAMERA_MARGIN_RIGHT), - 1536.0 + GetCameraMargin(CAMERA_MARGIN_BOTTOM))
call SetDayNightModels("Environment\\DNC\\DNCLordaeron\\DNCLordaeronTerrain\\DNCLordaeronTerrain.mdl", "Environment\\DNC\\DNCLordaeron\\DNCLordaeronUnit\\DNCLordaeronUnit.mdl")
call NewSoundEnvironment("Default")
call SetAmbientDaySound("IceCrownDay")
call SetAmbientNightSound("IceCrownNight")
call SetMapMusic("Music", true, 0)
call CreateRegions()
call CreateAllUnits()
call InitBlizzard()



call ExecuteFunc("cjLibw560nbs9b8nse46703948__init")

call InitGlobals()
call InitCustomTriggers()
endfunction
function config takes nothing returns nothing
call SetMapName("")
call SetMapDescription("没有说明")
call SetPlayers(2)
call SetTeams(2)
call SetGamePlacement(MAP_PLACEMENT_USE_MAP_SETTINGS)
call DefineStartLocation(0, 0.0, - 384.0)
call DefineStartLocation(1, - 768.0, - 192.0)
call InitCustomPlayerSlots()
call SetPlayerSlotAvailable(Player(0), MAP_CONTROL_USER)
call SetPlayerSlotAvailable(Player(1), MAP_CONTROL_COMPUTER)
call InitGenericPlayerSlots()
endfunction



//Struct method generated initializers/callers:

