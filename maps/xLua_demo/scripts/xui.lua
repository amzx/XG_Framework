local mouse = require 'xLua.module.Mouse'
button = xui.button:new(
    {
        w = 0.07,
        h = 0.04,
        x = 0.9,
        y = 0.2,
    }
)
button2 = xui.button:new(
    {
        w = 0.07,
        h = 0.04,
        x = 0.9,
        y = 0.28,
        value = '恢复满血',
    }
)

button.value = '生成一个敌人'

button:event('click', on_button_1_click)
button2:event('click', on_button_2_click)

---测试是否会发生异步
button3 = xui.button:new(
    {
        w = 0.07,
        h = 0.04,
        x = 0.9,
        y = 0.34,
        value = '垃圾回收',
    }
)
button3:event('click', on_button_3_click)

panel = xui.panel:new(
    {
        x = 0.5,
        y = 0.3,
        w = 0.2,
        h = 0.2,
        value = 0.3,
        --滚动条
        hasScrollBar = true,
        --关闭按钮
        --hasCloseButton = true,
    }

)

--扩展一页[默认]
panel.frame_vScroll.max = 1.0

button3 = xui.button:new(
    {
        parent = panel,
        w = 0.3,
        h = 0.1,
        x = 0.2,
        y = 0.08,
        value = '000',
    }
)

button3:event( 'enter',
    function (btn)
        print( '000 鼠标移入' )
    end
)

button3:event( 'leave',
    function (btn)
        print( '000 鼠标离开' )
    end
)

button3:event( 'click',
    function (btn)
        print( '000被点击' )
    end
)

icon1 = xui.icon:new(
    {
        parent = panel,
        w = 0.3,
        h = 0.3,
        x = 0.2,
        y = 0.8,
        image = "ReplaceableTextures\\CommandButtons\\BTNManual3.blp",
        --value = '111',
    }
)
icon1:addTag('canDrag')


cb = xui.checkbox:new(
    {
        parent = panel,
        w = 0.5,
        h = 0.1,
        x = 0.2,
        y = 1.5,
        value = 'checkbox11111',
    }
)
rb = xui.radiobox:new(
    {
        parent = panel,
        w = 0.3,
        h = 0.1,
        x = 0.2,
        y = 1.2,
        value = '222',
    }
)

dragICO = xui.icon:new(
    {
        parent = nil,
        w = xui:a2rH(24),
        h = xui:a2rV(24),
        x = 0.2,
        y = 0.6,
        image = "ReplaceableTextures\\CommandButtons\\BTNManual3.blp",
        visible = false,
        enable = false,
    }
)

local t
local dragCallback = function (t)
    dragICO.x = mouse.x - 0.01
    dragICO.y = mouse.y - 0.01
end

xui.event:setDragCallback( function (obj, frame, x, y)
    if not obj then
        --排除原生UI
        return
    end
    if not obj:hasTag('canDrag') then
        return
    end
    print('drag')
    dragICO.visible = true

    t = timer:loop( 0.01, dragCallback )
    dragCallback(t)
    return true
end )

xui.event:setDropCallback( function (obj, frame, x, y)
    print('drop')
    if t then
        t:del()
        t = nil
    end
    dragICO.visible = false
end )

--panel.visible = false

local flw = xui.button:new(
    {
        w = 0.07,
        h = 0.04,
        x = 0.5,
        y = 0.28,
        value = '111111111',
        visible = false,
    }
)


do return end
timer:loop( 0.01,function (t)

    local p = {
        x = hero:getX(),
        y = hero:getY(),
        z = hero:getZ()
    }

    p = camera.WorldToScreen( p )

    flw.x = p.x
    flw.y = p.y
    
    
    --japi.DzFrameClearAllPoints( flw.frame_background.frame )
    --japi.DzFrameSetPoint( flw.frame_background.frame, 6, xconst.frame.GameUI, 6, p.x, p.y  )
    --japi.DzFrameSetSize( flw.frame_background.frame , 0.2, .2 )
    
    --japi.DzFrameSetPoint( flw.frame_background.frame, POINT_RB, shell.frame_background.frame, constFrame.POINT_RB, 0, 0  )
end)