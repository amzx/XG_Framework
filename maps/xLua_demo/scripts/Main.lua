---@diagnostic disable: missing-parameter
require 'xLua.Main' --载入框架

local rct = xrect:n2o( '*区域' )

--使右边的区域可见
fog:rect( {
    player = player[1],
    state = 'visible',
    rect = rct,
} )
---@type unit 强行转化类型 才能看到一些扩展函数的注释
hero = unit:new({
    player = player[1],
    id = '*玛维',
})
local hero = hero

print( 'Hello xLua' )
printf(
    '<雪月框架>\n当前环境:%s\n当前版本:%s',
    xLua.Env,
    xLua.Version
)
player[1]:msg( '按下F3 热更新xui.lua' )

hero:addItem( '*药水' )
local it = hero:addItem( '*攻击' )

on_button_2_click  = function (btn)
    sync:new(player.self.id):Send('F5')
end

sync:callback( 'F5', function()
    hero:setHPprecent( 1.0 )
end)

on_button_1_click  = function (btn)
    local pid = 2
    sync:new(pid):Send('F2')
end

sync:callback( 'F2', function()
    data = sync:load()
    unit:new({
        player = player[ data[1] ],
        id = '*玛维',
    })
end)

local mouse = require 'xLua.module.Mouse'


event_f5 = keyboard:hotkey( '键盘-按下',
    0,
    xconst.keyboard.KEY_F5,
    function (event, params)
        --同步
        xui.button:new{
        w = 0.07,
        h = 0.04,
        x = mouse.x,
        y = mouse.y,
        value = 'XXX',
        }

    end
)

local japi = japi

require 'xLua.module.ui'

require 'xui'
event_f3 = keyboard:hotkey( '键盘-按下',
    0,
    xconst.keyboard.KEY_F3,
    function (event, params)
        --更新UI
        print('---更新UI---')
        xui:reset()
        hotFix('xui')
        collectgarbage("collect")
    end
)

--for key, value in pairs(debug.getinfo(japi.DzClearJassStringNotReference, 'l')) do
--    print(  key, value)
--end

