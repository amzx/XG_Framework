require 'xslk.rules'
xslk.item:new {
    _parent = 'pman',

    Name = '用也用不完的魔法药水',

    --使用完全消失
    preishable = 0,
    uses = 0,

    cooldownID = '',
}

xslk.item:new {
    _parent = 'pman',

    Name = '攻击',
    desc = [[@ATK1@
@ATK2@
@HP1@
@ARM1@
]],
    ATTR = {
        { 'ATK1', '攻击-10' },
        { 'ATK2', '攻击+33' },
        { 'ARM1', '护甲+22' },
        { 'HP1', '生命+40' },
    },

    --使用完全消失
    preishable = 0,
    uses = 0,

    cooldownID = '',
    Art = [[ReplaceableTextures\CommandButtons\BTNClawsOfAttack.blp]],
    
}