xslk.item:rule {
    desc = function (params)
        local atk = params[1]
        local sign = tonumber(atk)>0 and '+' or ''

        return ( '%s%d 攻击' ):format( sign, atk )
    end,
    --匹配ATTR 传入的参数为 ATTR中的第二个值
    match = function ( desc )
        value = desc:match('攻击%s-([%+%-]%s-%d+)')
        if value and value:sub(1,1) == '+' then
            value = value:sub(2)
        end
        return value
    end,
    --标识 技能|模拟|混合
    --使用技能则表示完全使用技能数据,为每个属性创建技能
    --使用模拟则表示完全由作者模拟属性，系统不会自动处理这些数据
    --使用混合则表示技能+修改数据以最少技能达到效果
    flag = '混合',

    --nil|string|ability 预设技能
    abil = 'AItg',
    data = 'DataA',
}

xslk.item:rule {
    desc = function (params)
        local atk = params[1]
        local sign = tonumber(atk)>0 and '+' or ''

        return ( '%s%d 护甲' ):format( sign, atk )
    end,
    --匹配ATTR 传入的参数为 ATTR中的第二个值
    match = function ( desc )
        value = desc:match('护甲%s-([%+%-]%s-%d+)')
        if value and value:sub(1,1) == '+' then
            value = value:sub(2)
        end
        return value
    end,
    --标识 技能|模拟|混合
    --使用技能则表示完全使用技能数据,为每个属性创建技能
    --使用模拟则表示完全由作者模拟属性，系统不会自动处理这些数据
    --使用混合则表示技能+修改数据以最少技能达到效果
    flag = '混合',

    --nil|string|ability 预设技能
    abil = 'AId1',
    data = 'DataA',
}

xslk.item:rule {
    desc = function (params)
        local atk = params[1]
        local sign = tonumber(atk)>0 and '+' or ''

        return ( '%s%d 生命值' ):format( sign, atk )
    end,
    --匹配ATTR 传入的参数为 ATTR中的第二个值
    match = function ( desc )
        value = desc:match('生命%s-([%+%-]%s-%d+)')
        if value and value:sub(1,1) == '+' then
            value = value:sub(2)
        end
        return value
    end,

    --生命值、法力值无法用混合的方法；有些技能更改数据是不生效的
    flag = '技能',

    --nil|string|ability 预设技能
    abil = 'AIl2',
    data = 'DataA',
}
--[[ 
--当你使用模拟的时候你需要自定义一个模拟函数 每个具有模拟属性的物品都会经过这个函数处理
xslk.item.method['模拟'] = function ( rule, item )
    
end
]]