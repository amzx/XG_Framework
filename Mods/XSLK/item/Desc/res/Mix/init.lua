--混合物编
--初始化

--遍历当前所有单位的物品 初始化属性
--单位获得物品时 获取物品属性 修改单位属性


--单位失去物品/物品被删除 修改单位属性

local rules = xslk.item.rules
local EXGetUnitAbility = japi.EXGetUnitAbility
local EXSetAbilityDataReal = japi.EXSetAbilityDataReal
local SetUnitAbilityLevel = cj.SetUnitAbilityLevel
local GetUnitAbilityLevel = cj.GetUnitAbilityLevel
local iid = base.iid

local data = {
    DataA = 108,
    DataB = 109,
    DataC = 110,
    DataD = 111,
    DataE = 112,
    DataF = 113,
    DataG = 114,
    DataH = 115,
    DataI = 116,
}

unit:event_any(
    "单位-获得物品",
    function (event, params)
        local item = params.item
        ---@type unit
        local u = params.unit

        item.holder = u

        local slk = item.slk
        local ATTRS = slk.ATTR
        local value
        if ATTRS then

            for _, ATTR in ipairs(ATTRS) do
                
                if not ATTR[3] then
                    for index, rule in ipairs(rules) do
                        value = rule.match( ATTR[2] )
                        if value then
                            ATTR[3] = rule
                        end
                    end
                end
                local rule = ATTR[3]
                if rule then

                    if not u.ATTR then
                        u.ATTR = {}
                    end
                    local orig = u.ATTR[rule.abil] or 0
                    value = rule.match( ATTR[2] ) + orig
                    u.ATTR[rule.abil] = value
                    
                    local abilid = iid(rule.abil)

                    local lv = GetUnitAbilityLevel(u.handle,abilid)

                    if lv == 0 then
                        --print("技能不存在！")
                        return
                    elseif lv == 1 then
                        lv = 2
                    else
                        lv = 1
                    end
                    EXSetAbilityDataReal( EXGetUnitAbility(u.handle, abilid), lv, data[rule.data], value )
                    SetUnitAbilityLevel( u.handle, abilid, lv )
                end
            end

        end
    end
)
local removeATTR = function ( params )
    
    local item = params.item
    ---@type unit
    local u = params.unit

    item.holder = nil

    local slk = item.slk
    local ATTRS = slk.ATTR
    local value
    if ATTRS then

        for _, ATTR in ipairs(ATTRS) do
            
            if not ATTR[3] then
                for index, rule in ipairs(rules) do
                    value = rule.match( ATTR[2] )
                    if value then
                        ATTR[3] = rule
                    end
                end
            end
            local rule = ATTR[3]
            if rule then

                if not u.ATTR then
                    u.ATTR = {}
                end
                local orig = u.ATTR[rule.abil] or 0
                value =  orig - rule.match( ATTR[2] )
                u.ATTR[rule.abil] = value
                
                local abilid = iid(rule.abil)

                local lv = GetUnitAbilityLevel(u.handle,abilid)

                if lv == 0 then
                    --print("技能不存在！")
                    return
                elseif lv == 1 then
                    lv = 2
                else
                    lv = 1
                end
                EXSetAbilityDataReal( EXGetUnitAbility(u.handle, abilid), lv, data[rule.data], value )
                SetUnitAbilityLevel( u.handle, abilid, lv )
            end
        end

    end

end

unit:event_any(
    "单位-丢弃物品",
    function (event, params)
        removeATTR(params)
    end
)



unit:event_any(
    "单位-初始化",
    function (event, params)
        local item = params.item
        ---@type unit
        local u = params.unit

        for index, rule in ipairs(rules) do
            if rule.flag == '混合' then
                u:addAbil( rule.abil )
                EXSetAbilityDataReal( EXGetUnitAbility(u.handle, iid(rule.abil)), 2, data[rule.data], 0 )
                SetUnitAbilityLevel( u.handle, iid(rule.abil), 2 )
            end
        end

    end
)

hook['RemoveItem'] = function (whichItem)
    local it = item:h2o(whichItem)
    if it.holder then
        removeATTR({
            unit = it.holder,
            item = it
        })
    end
end