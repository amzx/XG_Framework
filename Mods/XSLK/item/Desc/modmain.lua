---@type cMod
local mt = {
    type = 'class',
    class = 'mod',

}
local table_insert = table_insert
---全局变量中包括modinfo表中定义的值





function mt:inject()
    local code = {}
    --注入lua
    if self.Mix then
        table_insert(code, ('require "Mods.%s.Mix"\n'):format(self.modName:gsub('\\','.')))
        self:importFolder( "Mix", ('Mods\\%s\\Mix'):format(self.modName) )
    end

    --覆盖init
    self:importFileByStr( ('Mods\\%s\\Init.lua'):format(self.modName), table.concat(code) )

end

--Mod入口函数
function mt:Main()

    self:startup('Startup\\init.lua')


    
end

return mt