-- luac -o [output文件路径] [input文件路径]

---@type cMod
local mt = {
    type = 'class',
    class = 'mod',

}

local exe
local table_insert = table_insert
local table_concat = table.concat
function mt:encryption()
    local list = dir_list( 
        Root .. '\\debug\\' .. builder.map .. '\\temp\\map',
        true,
        '*.lua /a-d'
        )
    local cmds = {}
    local cmd
    table_insert( cmds, '@echo off' )
    for index, lua_path in ipairs(list) do
        lua_path = lua_path
        cmd = ('"%s" -o "%s" "%s"'):format( exe:u2a(), lua_path, lua_path, lua_path)
        table_insert( cmds, cmd )
        --lua直接运行代码 luac识别不了lua_path
    end
    local bat = self:getModPath() .. '\\temp.bat'
    write(bat, table_concat(cmds,'\n'))
    runCMD(bat,true)
    delete(bat)
end




function mt:Main()
    print('【Mod】' .. Name ..'v' .. Version ..' 加载成功' )
    exe = self:getModPath() .. '\\luac.exe'
end



return mt